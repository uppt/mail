package de.maltebehrendt.uppt.services;

import com.sun.mail.imap.IMAPFolder;
import de.maltebehrendt.uppt.annotation.Payload;
import de.maltebehrendt.uppt.annotation.Processor;
import de.maltebehrendt.uppt.enums.DataType;
import io.vertx.core.*;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mail.*;

import javax.mail.*;
import javax.mail.event.MessageCountAdapter;
import javax.mail.event.MessageCountEvent;
import javax.mail.internet.MimeBodyPart;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Pattern;

public class MessagingMailService1 extends AbstractService {

    private ConcurrentHashMap<String, JsonObject> accountConfigMap = null;
    private ConcurrentHashMap<String, Pattern> emailWhitelistPatternMap = null;
    private ConcurrentHashMap<String, MailClient> mailClientMap = null;
    private ConcurrentHashMap<String, IMAPFolder> monitorFolderMap = null;
    private ConcurrentHashMap<String, JsonObject> monitorConfigMap = null;


    // data structure is set up so that messages are always attached to an account successfully
    // regarding threads and correspondences, running conditions might cause inconsistencies which currently might do not get cleaned up
    private static String AQL_ensureAccountDocumentEntry = "WITH accounts, last_account_message LET lamE = (FOR e IN last_account_message FILTER e._from == @accountKey LIMIT 1 RETURN {id: e._id, key: e._key}) UPSERT {_key: @accountKey} INSERT {_key: @accountKey, createdTimestamp: @timestamp, updatedTimestamp: @timestamp} UPDATE {updatedTimestamp: @timestamp} IN accounts RETURN {id: NEW._id, key: NEW._key, lastAccountMessage: lamE}";
    private static String AQL_ensureLastAccountMessageEntry = "WITH last_account_message, messages LET msgD = (INSERT {accountKey: @accountKey} IN messages RETURN {id: NEW._id, key: NEW._key}) LET lamE = (INSERT {_from: @accountID, _to: FIRST(msgD).id} IN last_account_message RETURN {id: NEW._id, key: NEW._key}) RETURN {messageID: FIRST(msgD).id, messageKey: FIRST(msgD).key, lastAccountMessageKey: FIRST(lamE).key, lastAccountMessageID: FIRST(lamE).id}";
    private static String AQL_checkBeforeInsert = "WITH messages, threads LET thread = (FOR t IN threads FILTER t._key == @threadDocumentKey AND t.accountKey == @accountKey LIMIT 1 RETURN {threadDocumentKey: t._key, threadDocumentID: t._id}) LET duplicate = (FOR m IN messages FILTER m.messageID == @messageID AND m.accountKey == @accountKey LIMIT 1 RETURN {messageDocumentKey: m._key, messageDocumentID: m._id, threadDocumentKey: m.threadDocumentKey}) LET reference = (FOR m IN messages FILTER m.messageID == @inReplyTo AND m.accountKey == @accountKey LIMIT 1 RETURN {messageDocumentKey: m._key, messageDocumentID: m._id, threadDocumentKey: m.threadDocumentKey}) RETURN {reference: reference, duplicate: duplicate, thread: thread}";
    private static String AQL_insertNewMessageAndThread = "WITH messages,threads,correspondences,last_thread_message,has_thread,last_account_message, next_account_message LET cD = (UPSERT {accountKey: @accountKey, correspondentsHash: @correspondentsHash} INSERT {accountKey: @accountKey, correspondentsHash: @correspondentsHash, lastChangeTimestamp: @timestamp} UPDATE {lastChangeTimestamp: @timestamp} IN correspondences RETURN {id: NEW._id, key: NEW._key, wasExisting: OLD? true : false}) LET tD = (INSERT {accountKey: @accountKey, lastChangeTimestamp: @timestamp, title: @threadTitle} IN threads RETURN {id: NEW._id, key: NEW._key}) LET msgD = (INSERT MERGE(@messageDocument, {correspondenceDocumentKey: FIRST(cD).key, threadDocumentKey: FIRST(tD).key}) IN messages RETURN {id: NEW._id, key: NEW._key}) LET lamE = (FOR e IN last_account_message FILTER e._from == @accountID LIMIT 1 RETURN {key: e._key, id: e._id}) LET lamEu = (UPDATE {_key: FIRST(lamE).key} WITH {_to: FIRST(msgD).id} IN last_account_message RETURN {id: NEW._id, key: NEW._key, oldLastAccountMessageID: OLD._to}) LET namE = (INSERT {_from: FIRST(msgD).id, _to: FIRST(lamEu).oldLastAccountMessageID} IN next_account_message RETURN {id: NEW._id, key: NEW._key}) LET htE = (INSERT {_from: @accountID, _to: FIRST(tD).id} IN has_thread RETURN {id: NEW._id, key: NEW._key}) LET ltmE = (INSERT {_from: FIRST(tD).id, _to: FIRST(msgD).id} IN last_thread_message RETURN {id: NEW._id, key: NEW._key}) RETURN {messageID: FIRST(msgD).id, messageKey: FIRST(msgD).key, threadID: FIRST(tD).id, threadKey: FIRST(tD).key, hasThreadID: FIRST(htE).id, hasThreadKey: FIRST(htE).key, lastThreadMessageID: FIRST(ltmE).id, lastThreadMessageKey: FIRST(ltmE).key, lastAccountMessageKey: FIRST(lamE).key, lastAccountMessageID: FIRST(lamE).id, nextAccountMessageID: FIRST(namE).id, nextAccountMessageKey: FIRST(namE).key, correspondenceDocumentID: FIRST(cD).id, correspondenceDocumentKey: FIRST(cD).key, correspondenceDocumentWasExisting: FIRST(cD).wasExisting}";
    private static String AQL_insertNewMessageUpdateThread = "WITH messages, threads, correspondences, last_account_message, next_account_message, last_thread_message, next_thread_message LET cD = (UPSERT {accountKey: @accountKey, correspondentsHash: @correspondentsHash} INSERT {accountKey: @accountKey, correspondentsHash: @correspondentsHash, lastChangeTimestamp: @timestamp} UPDATE {lastChangeTimestamp: @timestamp} IN correspondences RETURN {id: NEW._id, key: NEW._key, wasExisting: OLD? true : false}) LET tD = (UPDATE {_key: @threadDocumentKey} WITH {lastChangeTimestamp: @timestamp} IN threads RETURN {id: NEW._id, key: NEW._key}) LET msgD = (INSERT MERGE(@messageDocument, {correspondenceDocumentKey: FIRST(cD).key, threadDocumentKey: @threadDocumentKey}) IN messages RETURN {id: NEW._id, key: NEW._key}) LET lamE = (FOR e IN last_account_message FILTER e._from == @accountID LIMIT 1 RETURN {key: e._key, id: e._id}) LET lamEu = (UPDATE {_key: FIRST(lamE).key} WITH {_to: FIRST(msgD).id} IN last_account_message RETURN {id: NEW._id, key: NEW._key, oldLastAccountMessageID: OLD._to}) LET namE = (INSERT {_from: FIRST(msgD).id, _to: FIRST(lamEu).oldLastAccountMessageID} IN next_account_message RETURN {id: NEW._id, key: NEW._key}) LET ltmE = (FOR e IN last_thread_message FILTER e._from == FIRST(tD).id LIMIT 1 RETURN {key: e._key, id: e._id}) LET ltmEu = (UPDATE {_key: FIRST(ltmE).key} WITH {_to: FIRST(msgD).id} IN last_thread_message RETURN {id: NEW._id, key: NEW._key, oldLastThreadMessageID: OLD._to}) LET ntmE = (INSERT {_from: FIRST(msgD).id, _to: FIRST(ltmEu).oldLastThreadMessageID} IN next_thread_message RETURN {id: NEW._id, key: NEW._key}) RETURN {messageID: FIRST(msgD).id, messageKey: FIRST(msgD).key, threadID: FIRST(tD).id, threadKey: FIRST(tD).key, lastAccountMessageKey: FIRST(lamE).key, lastAccountMessageID: FIRST(lamE).id, nextAccountMessageID: FIRST(namE).id, nextAccountMessageKey: FIRST(namE).key, correspondenceDocumentID: FIRST(cD).id, correspondenceDocumentKey: FIRST(cD).key, correspondenceDocumentWasExisting: FIRST(cD).wasExisting, lastThreadMessageID: FIRST(ltmE).id, lastThreadMessageKey: FIRST(ltmE).key, nextThreadMessageID: FIRST(ntmE).id, nextThreadMessageKey: FIRST(ntmE).key, threadID: FIRST(tD).id, threadKey: FIRST(tD).key}";
    private static String AQL_insertCorrespondenceLinks = "WITH correspondences, last_correspondence_message, has_correspondence LET cD = (UPDATE {_key: @correspondenceDocumentKey} WITH {correspondents: @correspondents} IN correspondences RETURN {id: NEW._id, key: NEW._key}) LET hcE = (INSERT {_from: @accountID, _to: @correspondenceDocumentID} IN has_correspondence RETURN {id: NEW._id, key: NEW._key}) LET lcmE = (INSERT {_from: @correspondenceDocumentID, _to: @messageDocumentID} IN last_correspondence_message) RETURN {correspondenceDocumentID: FIRST(cD).id, correspondenceDocumentKey: FIRST(cD).key, hasCorrespondenceID: FIRST(hcE).id, hasCorrespondenceKey: FIRST(hcE).key, lastCorrespondenceID: FIRST(lcmE).id, lastCorrespondenceKey: FIRST(lcmE).key}";
    private static String AQL_updateCorrespondenceLinks = "WITH last_correspondence_message, next_correspondence_message LET lcmE = (FOR e IN last_correspondence_message FILTER e._from == @correspondenceDocumentID LIMIT 1 RETURN {key: e._key, id: e._id}) LET lcmEu = (UPDATE {_key: FIRST(lcmE).key} WITH {_to: @messageDocumentID} IN last_correspondence_message RETURN {id: NEW._id, key: NEW._key, oldLastCorrespondenceMessageID: OLD._to}) LET ncmE = (INSERT {_from: @messageDocumentID, _to: FIRST(lcmEu).oldLastCorrespondenceMessageID} IN next_correspondence_message RETURN {id: NEW._id, key: NEW._key}) RETURN {lastCorrespondenceMessageID: FIRST(lcmE).id, lastCorrespondenceMessageKey: FIRST(lcmE).key, nextCorrespondenceMessageID: FIRST(ncmE).id, nextCorrespondenceMessageKey: FIRST(ncmE).key}";

    private static String AQL_identifyMissingImapIDs = "WITH messages LET persistedImapIDs = (FOR imapID IN @imapIDs FOR m IN messages FILTER m.accountKey == @accountKey AND m.imapID == imapID RETURN m.imapID) RETURN {missingImapIDs: MINUS(@imapIDs, persistedImapIDs)}";

    private static String AQL_getMessagesOfAccountViaFirstAccountMessage = "WITH messages, last_account_message, next_account_message FOR m IN 1..@maxMessages OUTBOUND @accountID next_account_message, last_account_message FILTER HAS(m, \"messageID\") RETURN m";
    private static String AQL_getMessagesOfAccountViaStartMessage = "WITH messages, next_account_message FOR m IN 1..@maxMessages OUTBOUND @startEmailDocumentID next_account_message FILTER HAS(m, \"messageID\") RETURN m";
    private static String AQL_getMessagesOfCorrespondenceViaFirstCorrespondenceMessage = "WITH messages, last_correspondence_message, next_correspondence_message FOR m IN 1..@maxMessages OUTBOUND @correspondenceDocumentID next_correspondence_message, last_correspondence_message FILTER m.accountKey == @accountKey AND HAS(m, \"messageID\") RETURN m";
    private static String AQL_getMessagesOfCorrespondenceViaStartMessage = "WITH messages, next_correspondence_message FOR m IN 1..@maxMessages OUTBOUND @startEmailDocumentID next_correspondence_message FILTER m.correspondenceDocumentKey == @correspondenceDocumentKey AND m.accountKey == @accountKey AND HAS(m, \"messageID\") RETURN m";
    private static String AQL_getMessagesOfThreadViaFirstThreadMessage = "WITH messages, last_thread_message, next_thread_message FOR m IN 1..@maxMessages OUTBOUND @threadDocumentID next_thread_message, last_thread_message FILTER m.accountKey == @accountKey AND HAS(m, \"messageID\") RETURN m";
    private static String AQL_getMessagesOfThreadViaStartMessage = "WITH messages, next_thread_message FOR m IN 1..@maxMessages OUTBOUND @startEmailDocumentID next_thread_message FILTER m.threadDocumentKey == @threadDocumentKey AND m.accountKey == @accountKey AND HAS(m, \"messageID\") RETURN m";




    // TODO: check smtp credentials upon prepare
    // TODO: use storage for payloads
    // TODO: use shared config maps like bridgeWeb in order to be scaleable
    // TODO: offer new notification -> sending via userID (retrieve mail from users service)
    // TODO: use canaryTimestamp for detecting broken monitors
    // TODO: delete mails concerning users/delete mail addresses


    @Processor(
            domain = "messagingEmail",
            version = "1",
            type = "newAccountAvailable",
            description = "Adds an account/makes sure the account is setup (so it can be used for sending and/monitoring mails)",
            enablingConditionKey = "isAddingAccountsEnabled",
            requires = {
                @Payload(key = "accountKey", type = DataType.STRING, description = "Unique identifier/key of the account, e.g. the mail address"),
                @Payload(key = "serverSettings", type = DataType.JSONObject, description = "Server settings, e.g. 'emailAddress', 'smtpHost','smtpPort','imapHost','imapPort','user','password','isStartTLSRequired','isServerCertCheckRequired'"),
            },
            optional = {
                    @Payload(key ="isSendingEnabled", type = DataType.BOOLEAN, description = "Must be true, if account shall be used for sending mails"),
                    @Payload(key ="sendingSettings", type = DataType.JSONObject, description = "Sending setting, e.g. 'whitelistByServiceOrigin', 'emailWhitelistPattern', 'ehloHostname', 'isAllowRcptErrors'"),
                    @Payload(key ="isMonitoringEnabled", type = DataType.BOOLEAN, description = "Must be true, if account shall be used for receiving mails"),
                    @Payload(key ="monitoringSettings", type = DataType.JSONObject, description = "Monitoring/receiving settings, e.g. 'whitelistByServiceOrigin', 'folderName', 'notificationAddresses'")
            }
    )
    public void processNewAccountAvailable(de.maltebehrendt.uppt.events.Message message) {
        // check if origin is allowed to add accounts
        JsonArray whitelistByServiceOrigin = config().getJsonObject("addingAccountSettings").getJsonArray("whitelistByServiceOrigin");
        if(!whitelistByServiceOrigin.contains("*") && !whitelistByServiceOrigin.contains(message.origin())) {
            logger.security(message.correlationID(), message.origin(), 403, "Adding account failed - origin not in 'whitelistByServiceOrigin'");
            message.fail(403, "Adding account failed - origin not in 'whitelistByServiceOrigin'");
            return;
        }

        JsonObject accountSettings = message.getBodyAsJsonObject();

        Future<Object> setupFuture = Future.future();
        setupFuture.setHandler(setupResult -> {
            if(setupResult.failed()) {
                logger.error(message.correlationID(), 400, "Failed to add account!", setupResult.cause());
                message.fail(400, "Failed to add account: " + setupResult.cause().getMessage());
                return;
            }

            if(accountSettings.containsKey("isMonitoringEnabled") && accountSettings.getBoolean("isMonitoringEnabled")) {
                String monitorKey = accountSettings.getJsonObject("monitoringSettings").getString("folderName") + "@" + accountSettings.getString("accountKey");

                Future<Boolean> monitoringFuture = Future.future();
                monitoringFuture.setHandler(monitoringResult -> {
                    if(monitoringResult.failed()) {
                        logger.error(message.correlationID(), 400, "Failed to start monitoring of new account " + accountSettings.getString("accountKey"), monitoringResult.cause());
                        message.fail(500, "Failed to start monitoring of new account " + accountSettings.getString("accountKey"));
                        return;
                    }
                    message.reply(200);
                });
                startMonitoringOfImapFolder(message.correlationID(), monitorKey, monitoringFuture);
            }
            else {
                message.reply(200);
            }
        });
        setupAccount(accountSettings, setupFuture);
    }


    @Processor(
            domain = "messagingEmail",
            version = "1",
            type = "emailsOfCorrespondentsRequired",
            description = "Retrieves email messages of a group of correspondents in the order they have been persisted in the database",
            requires = {
                    @Payload(key = "correspondenceDocumentKey", type = DataType.STRING, description = "Database document key of the required correspondence")
            },
            optional = {
                    @Payload(key = "accountKey", type = DataType.STRING, description = "Key/name of the mail account. If not provided, 'defaultAccount' is used"),
                    @Payload(key = "startEmailDocumentKey", type = DataType.STRING, description = "Offset. Email document key (in database) which preceeding emails are to be received. If not provided, the latest mail of the account is used"),
                    @Payload(key = "maxEmails", type = DataType.LONG, description = "Max number of emails to be retrieved. If not provided, 25 is used")
            },
            provides = {
                    @Payload(key = "mails", type = DataType.JSONArray, description = "Array of mails, each entry is a mail JsonObject (payload, envelope)")
            }
    )
    public void processEmailsOfCorrespondentsRequired(de.maltebehrendt.uppt.events.Message message) {
        JsonObject body = message.getBodyAsJsonObject();

        String correspondenceDocumentKey = body.getString("correspondenceDocumentKey");
        if(correspondenceDocumentKey == null || correspondenceDocumentKey.isEmpty()) {
            message.fail(400, "Must provide non-empty 'correspondenceDocumentKey'");
            return;
        }

        String accountKey = body.containsKey("accountKey") ? body.getString("accountKey") : "defaultAccount";
        String startEmailDocumentKey = body.getString("startEmailDocumentKey");
        Long maxEmails = body.getLong("maxEmails");

        if(maxEmails == null || maxEmails < 1L) {
            maxEmails = 25L;
        }

        JsonObject serverConfig = accountConfigMap.get(accountKey);
        if(serverConfig == null) {
            logger.warn(message.correlationID(), message.origin(), 404, "Server config with " + accountKey + " + accountKey not found/setup!");
            message.fail(404, "Server config with " + accountKey + " + accountKey not found/setup!");
            return;
        }

        // check if origin is allowed to retrieve mail messages
        JsonArray whitelistByServiceOrigin = serverConfig.getJsonObject("monitoringSettings").getJsonArray("whitelistByServiceOrigin");
        if(!whitelistByServiceOrigin.contains("*") && !whitelistByServiceOrigin.contains(message.origin())) {
            logger.security(message.correlationID(), message.origin(), 403, "Retrieving mails failed - origin not in 'whitelistByServiceOrigin'");
            message.fail(403, "Retrieving mails failed - origin not in 'whitelistByServiceOrigin'");
            return;
        }

        Future<JsonArray> retrieveFuture = Future.future();
        retrieveFuture.setHandler(retrieveResult -> {
            if(retrieveResult.failed()) {
                logger.error(message.correlationID(), accountKey, 500, "Failed to retrieve emails of correspondence", retrieveResult.cause());
                message.fail(500,"Failed to retrieve mails of correspondence due to an unknown error");
                return;
            }

            JsonArray resultArray = retrieveResult.result();
            message.reply(new JsonObject().put("mails", resultArray));
        });
        HashMap<String, Object> paramMap = new HashMap<>();
        paramMap.put("accountKey", accountKey);
        paramMap.put("maxMessages", maxEmails);

        if(startEmailDocumentKey != null && !startEmailDocumentKey.isEmpty()) {
            paramMap.put("startEmailDocumentID", "messages/" + startEmailDocumentKey);
            paramMap.put("correspondenceDocumentKey", correspondenceDocumentKey);
            database.query(AQL_getMessagesOfCorrespondenceViaStartMessage, paramMap, retrieveFuture);
        }
        else {
            paramMap.put("correspondenceDocumentID", "correspondences/" + correspondenceDocumentKey);
            database.query(AQL_getMessagesOfCorrespondenceViaFirstCorrespondenceMessage, paramMap, retrieveFuture);
        }
    }

    @Processor(
            domain = "messagingEmail",
            version = "1",
            type = "emailsOfThreadRequired",
            description = "Retrieves email messages of a thread in the order they have been persisted in the database",
            requires = {
                    @Payload(key = "threadDocumentKey", type = DataType.STRING, description = "Database document key of the required thread")
            },
            optional = {
                    @Payload(key = "accountKey", type = DataType.STRING, description = "Key/name of the mail account. If not provided, 'defaultAccount' is used"),
                    @Payload(key = "startEmailDocumentKey", type = DataType.STRING, description = "Offset. Email document key (in database) which preceeding emails are to be received. If not provided, the latest mail of the account is used"),
                    @Payload(key = "maxEmails", type = DataType.LONG, description = "Max number of emails to be retrieved. If not provided, 25 is used")
            },
            provides = {
                    @Payload(key = "mails", type = DataType.JSONArray, description = "Array of mails, each entry is a mail JsonObject (payload, envelope)")
            }
    )
    public void processEmailsOfThreadRequired(de.maltebehrendt.uppt.events.Message message) {
        JsonObject body = message.getBodyAsJsonObject();

        String threadDocumentKey = body.getString("threadDocumentKey");
        if(threadDocumentKey == null || threadDocumentKey.isEmpty()) {
            message.fail(400, "Must provide non-empty 'threadDocumentKey'");
            return;
        }

        String accountKey = body.containsKey("accountKey") ? body.getString("accountKey") : "defaultAccount";
        String startEmailDocumentKey = body.getString("startEmailDocumentKey");
        Long maxEmails = body.getLong("maxEmails");

        if(maxEmails == null || maxEmails < 1L) {
            maxEmails = 25L;
        }

        JsonObject serverConfig = accountConfigMap.get(accountKey);
        if(serverConfig == null) {
            logger.warn(message.correlationID(), message.origin(), 404, "Server config with " + accountKey + " + accountKey not found/setup!");
            message.fail(404, "Server config with " + accountKey + " + accountKey not found/setup!");
            return;
        }

        // check if origin is allowed to retrieve mail messages
        JsonArray whitelistByServiceOrigin = serverConfig.getJsonObject("monitoringSettings").getJsonArray("whitelistByServiceOrigin");
        if(!whitelistByServiceOrigin.contains("*") && !whitelistByServiceOrigin.contains(message.origin())) {
            logger.security(message.correlationID(), message.origin(), 403, "Retrieving mails failed - origin not in 'whitelistByServiceOrigin'");
            message.fail(403, "Retrieving mails failed - origin not in 'whitelistByServiceOrigin'");
            return;
        }

        Future<JsonArray> retrieveFuture = Future.future();
        retrieveFuture.setHandler(retrieveResult -> {
            if(retrieveResult.failed()) {
                logger.error(message.correlationID(), accountKey, 500, "Failed to retrieve emails of thread", retrieveResult.cause());
                message.fail(500,"Failed to retrieve mails of thread due to an unknown error");
                return;
            }

            JsonArray resultArray = retrieveResult.result();
            message.reply(new JsonObject().put("mails", resultArray));
        });
        HashMap<String, Object> paramMap = new HashMap<>();
        paramMap.put("accountKey", accountKey);
        paramMap.put("maxMessages", maxEmails);

        if(startEmailDocumentKey != null && !startEmailDocumentKey.isEmpty()) {
            paramMap.put("startEmailDocumentID", "messages/" + startEmailDocumentKey);
            paramMap.put("threadDocumentKey", threadDocumentKey);
            database.query(AQL_getMessagesOfThreadViaStartMessage, paramMap, retrieveFuture);
        }
        else {
            paramMap.put("threadDocumentID", "threads/" + threadDocumentKey);
            database.query(AQL_getMessagesOfThreadViaFirstThreadMessage, paramMap, retrieveFuture);
        }
    }


    @Processor(
            domain = "messagingEmail",
            version = "1",
            type = "emailsOfAccountRequired",
            description = "Retrieves email messages in the order they have been persisted in the database",
            optional = {
                    @Payload(key = "accountKey", type = DataType.STRING, description = "Key/name of the mail account. If not provided, 'defaultAccount' is used"),
                    @Payload(key = "startEmailDocumentKey", type = DataType.STRING, description = "Offset. Email document key (in database) which preceeding emails are to be received. If not provided, the latest mail of the account is used"),
                    @Payload(key = "maxEmails", type = DataType.LONG, description = "Max number of emails to be retrieved. If not provided, 25 is used")
            },
            provides = {
                    @Payload(key = "mails", type = DataType.JSONArray, description = "Array of mails, each entry is a mail JsonObject (payload, envelope)")
            }
    )
    public void processEmailsOfAccountRequired(de.maltebehrendt.uppt.events.Message message) {
        JsonObject body = message.getBodyAsJsonObject();

        String accountKey = body.containsKey("accountKey") ? body.getString("accountKey") : "defaultAccount";
        String startEmailDocumentKey = body.getString("startEmailDocumentKey");
        Long maxEmails = body.getLong("maxEmails");

        if(maxEmails == null || maxEmails < 1L) {
            maxEmails = 25L;
        }

        JsonObject serverConfig = accountConfigMap.get(accountKey);
        if(serverConfig == null) {
            logger.warn(message.correlationID(), message.origin(), 404, "Server config with " + accountKey + " + accountKey not found/setup!");
            message.fail(404, "Server config with " + accountKey + " + accountKey not found/setup!");
            return;
        }

        // check if origin is allowed to retrieve mail messages
        JsonArray whitelistByServiceOrigin = serverConfig.getJsonObject("monitoringSettings").getJsonArray("whitelistByServiceOrigin");
        if(!whitelistByServiceOrigin.contains("*") && !whitelistByServiceOrigin.contains(message.origin())) {
            logger.security(message.correlationID(), message.origin(), 403, "Retrieving mails failed - origin not in 'whitelistByServiceOrigin'");
            message.fail(403, "Retrieving mails failed - origin not in 'whitelistByServiceOrigin'");
            return;
        }

        Future<JsonArray> retrieveFuture = Future.future();
        retrieveFuture.setHandler(retrieveResult -> {
           if(retrieveResult.failed()) {
               logger.error(message.correlationID(), accountKey, 500, "Failed to retrieve emails of account", retrieveResult.cause());
               message.fail(500,"Failed to retrieve mails of account due to an unknown error");
               return;
           }

           JsonArray resultArray = retrieveResult.result();
           message.reply(new JsonObject().put("mails", resultArray));
        });
        HashMap<String, Object> paramMap = new HashMap<>();
        paramMap.put("maxMessages", maxEmails);

        if(startEmailDocumentKey != null && !startEmailDocumentKey.isEmpty()) {
            paramMap.put("startEmailDocumentID", "messages/" + startEmailDocumentKey);
            database.query(AQL_getMessagesOfAccountViaStartMessage, paramMap, retrieveFuture);
        }
        else {
            paramMap.put("accountID", "accounts/" + accountKey);
            database.query(AQL_getMessagesOfAccountViaFirstAccountMessage, paramMap, retrieveFuture);
        }
    }

    @Processor(
            domain = "messagingEmail",
            version = "1",
            type = "emailCreated",
            description = "Persists and sends a newly created email",
            requires = {
                    @Payload(key = "envelope", type = DataType.JSONObject, description = "TBD must contain 'toArray' (JsonArray), 'subject' (String); optionally 'inReplyTo' (String), 'referencesArray' (JsonArray), 'ccArray' (JsonArray), 'bccArray' (JsonArray), 'from' (String)"),
                    @Payload(key = "payload", type = DataType.JSONObject, description = "TBD must contain 'text' (String) and/or 'html' (String)")
            },
            optional = {
                    @Payload(key = "accountKey", type = DataType.STRING, description = "Key/name of the mail account to be used. Must be setup in advance. If not provided, 'defaultAccount' is used"),
                    @Payload(key = "threadDocumentKey", type = DataType.STRING, description = "Database document key of the corresponding thread. If not provided, either envelope.inReplyTo is used for identifying the thread or a thread is created (in no inReplyTo is provided)")
            },
            provides = {
                    @Payload(key = "messageID", type = DataType.STRING, description = "ID of the message, set by the mail client or mail server"),
                    @Payload(key = "correspondenceDocumentKey", type = DataType.STRING, description = "Key of the correspondence the message belongs to in the database"),
                    @Payload(key = "threadDocumentKey", type = DataType.STRING, description = "Key of the thread the message belongs to in the database"),
                    @Payload(key = "emailDocumentKey", type = DataType.STRING, description = "Key of the email message in the database"),
                    @Payload(key = "recipients", type = DataType.JSONArray, description = "Array of all of recipients the mail was sent to (if allowRcptErrors in config is true, this may be fewer than the intended recipients)")
            }
    )
    public void processEmailCreated(de.maltebehrendt.uppt.events.Message message) {
        JsonObject body = message.getBodyAsJsonObject();

        JsonObject envelope = body.getJsonObject("envelope");
        if(envelope == null || envelope.isEmpty()) {
            logger.warn(message.correlationID(), message.origin(), 400, "Empty 'envelope'");
            message.fail(400, "Must provide non-empty 'envelope' with at least 'toArray' and 'subject'");
            return;
        }
        JsonArray toArray = envelope.getJsonArray("toArray");
        if(toArray == null || toArray.isEmpty()) {
            logger.warn(message.correlationID(), message.origin(), 400, "Empty 'toArray'");
            message.fail(400, "Must provide non-empty 'envelope.toArray' with at least one email address");
            return;
        }
        String subject = envelope.getString("subject");
        if(subject == null || subject.isEmpty()) {
            logger.warn(message.correlationID(), message.origin(), 400, "Empty 'subject'");
            message.fail(400, "Must provide non-empty 'envelope.subject'");
            return;
        }

        JsonObject payload = body.getJsonObject("payload");
        if(payload == null || payload.isEmpty()) {
            logger.warn(message.correlationID(), message.origin(), 400, "Empty 'payload'");
            message.fail(400, "Must provide non-empty 'payload' with at least 'text' and/or 'html'");
            return;
        }
        String text = payload.getString("text");
        String html = payload.getString("html");
        if((text == null || text.isEmpty()) && (html == null || html.isEmpty())) {
            logger.warn(message.correlationID(), message.origin(), 400, "Empty 'text' and 'html'. At least one needs to be provided");
            message.fail(400, "Empty 'payload.text' and 'payload.html'. At least one needs to be provided");
            return;
        }

        String accountKey = body.getString("accountKey");
        if(accountKey == null || accountKey.isEmpty()) {
            accountKey = "defaultAccount";
        }
        envelope.put("accountKey", accountKey);

        JsonObject serverConfig = accountConfigMap.get(accountKey);
        if(serverConfig == null) {
            logger.warn(message.correlationID(), message.origin(), 404, "Server config with " + accountKey + " + accountKey not found/setup!");
            message.fail(404, "Server config with " + accountKey + " + accountKey not found/setup!");
            return;
        }

        // check if origin is allowed to send mail messages
        JsonArray whitelistByServiceOrigin = serverConfig.getJsonObject("sendingSettings").getJsonArray("whitelistByServiceOrigin");
        if(!whitelistByServiceOrigin.contains("*") && !whitelistByServiceOrigin.contains(message.origin())) {
            logger.security(message.correlationID(), message.origin(), 403, "Sending mail failed - origin not in 'whitelistByServiceOrigin'");
            message.fail(403, "Sending mail failed - origin not in 'whitelistByServiceOrigin'");
            return;
        }

        // take care of optional parameters
        String from = envelope.getString("from");
        if(from == null || from.isEmpty()) {
            from = serverConfig.getJsonObject("serverSettings").getString("emailAddress");
        }
        else {
            from = from.toLowerCase();
        }
        String fromAddress = from;

        String inReplyTo = envelope.getString("inReplyTo");
        String threadDocumentKey = body.getString("threadDocumentKey");
        JsonArray referencesArray = envelope.getJsonArray("referencesArray");
        JsonArray ccArray = envelope.getJsonArray("ccArray");
        JsonArray bccArray = envelope.getJsonArray("bccArray");

        if(threadDocumentKey != null && !threadDocumentKey.isEmpty()) {
            envelope.put("threadDocumentKey", threadDocumentKey);
        }

        // get the mail client
        MailClient mailClient = mailClientMap.get(accountKey);
        if(mailClient == null) {
            logger.warn(message.correlationID(), message.origin(), 404, "Mail client for accountKey " + accountKey + " not found/setup!");
            message.fail(404, "Mail client with accountKey " + accountKey + " not found/setup!");
            return;
        }
        Pattern emailPattern = emailWhitelistPatternMap.get(accountKey);

        MailMessage mailMessage = new MailMessage()
                .setFrom(fromAddress)
                .setSubject(subject);

        // converting to non-JSON lists...
        Iterator<Object> iterator = toArray.iterator();
        List<String> toRecipients = new LinkedList<String>();

        while(iterator.hasNext()) {
            String toRecipient = iterator.next().toString();
            if(!emailPattern.matcher(toRecipient).matches()) {
                logger.warn(message.correlationID(), message.origin(), 400, "'toArray' address " + toRecipient + " does not match email white list pattern!");
                message.fail(400, "'toArray' address " + toRecipient + " does not match email white list pattern!");
                return;
            }
            toRecipients.add(toRecipient);
        }
        mailMessage.setTo(toRecipients);

        if(ccArray != null && !ccArray.isEmpty()) {
            List<String> ccRecipients = new LinkedList<String>();
            iterator = ccArray.iterator();
            while(iterator.hasNext()) {
                String ccRecipient = iterator.next().toString();
                if(!emailPattern.matcher(ccRecipient).matches()) {
                    logger.warn(message.correlationID(), message.origin(), 400, "'cc' recipient address " + ccRecipient + " does not match email white list pattern!");
                    message.fail(400, "'cc' address " + ccRecipient + " does not match email white list pattern!");
                    return;
                }
                ccRecipients.add(ccRecipient);
            }
            mailMessage.setCc(ccRecipients);
        }
        if(bccArray != null && !bccArray.isEmpty()) {
            List<String> bccRecipients = new LinkedList<String>();
            iterator = bccArray.iterator();
            while(iterator.hasNext()) {
                String bccRecipient = iterator.next().toString();
                if(!emailPattern.matcher(bccRecipient).matches()) {
                    logger.warn(message.correlationID(), message.origin(), 400, "'bcc' recipient address " + bccRecipient + " does not match email white list pattern!");
                    message.fail(400, "'bcc' address " + bccRecipient + " does not match email white list pattern!");
                    return;
                }
                bccRecipients.add(bccRecipient);
            }
            mailMessage.setBcc(bccRecipients);
        }

        if(html != null && !html.isEmpty()) {
            mailMessage.setHtml(html);
        }
        if(text != null && !text.isEmpty()) {
            mailMessage.setText(text);
        }

        //TODO: work with string builder or sth similar more efficient than this
        //TODO: fold strings (new line after at most of 72 chars)
        if(inReplyTo != null && !inReplyTo.isEmpty()) {
            if(!inReplyTo.startsWith("<")) {
                inReplyTo = "<" + inReplyTo + ">";
            }
            mailMessage.addHeader("In-Reply-To", inReplyTo);
        }

        if(referencesArray != null && !referencesArray.isEmpty()) {
            String referencesString = null;

            if(!referencesArray.getString(0).startsWith("<")) {
                referencesString = "<" + referencesArray.getString(0) + ">";
            }
            else {
                referencesString = referencesArray.getString(0);
            }

            for(int i=1;i<referencesArray.size();i++) {
                if(!referencesArray.getString(i).startsWith("<")) {
                    referencesString += ",<" + referencesArray.getString(i) + ">";
                }
                else {
                    referencesString += "," + referencesArray.getString(+i);
                }
            }

            if(inReplyTo != null && !inReplyTo.isEmpty()) {
                referencesString += " " + inReplyTo;
            }

            mailMessage.addHeader("References", referencesString);
        }
        else if(inReplyTo != null && !inReplyTo.isEmpty()) {
            mailMessage.addHeader("References", inReplyTo);
        }


        mailClient.sendMail(mailMessage, result -> {
            if(result.failed()) {
                logger.error(message.correlationID(), message.origin(), 500, "Failed to send mail", result.cause());
                message.fail(500, "Failed to send mail due to an unknown error");
                return;
            }

            MailResult mailResult = result.result();

            JsonArray recipients = new JsonArray();
            for(String recipient : mailResult.getRecipients()) {
                recipients.add(recipient);
            }
            //TODO: check if number of recipients matches original request

            String messageID = mailResult.getMessageID();
            if(messageID.startsWith("<")) {
                messageID = messageID.substring(1, messageID.length()-1);
            }

            envelope.put("messageID", messageID)
                    .put("from", fromAddress);

            Future<JsonObject> persistFuture = Future.future();
            persistFuture.setHandler(persistResult -> {
                if(persistResult.failed()) {
                    logger.error(message.correlationID(), message.origin(), 500, "Failed to persist send email", persistResult.cause());
                    message.fail(500, "Failed to persist mail due to an unknown error");
                    return;
                }

                JsonObject resultObject = persistResult.result();

                message.reply(new JsonObject()
                        .put("messageID", envelope.getString("messageID"))
                        .put("recipients", recipients)
                        .put("emailDocumentKey", resultObject.getString("messageDocumentKey"))
                        .put("threadDocumentKey", resultObject.getString("threadDocumentKey"))
                        .put("correspondenceDocumentKey", resultObject.getString("correspondenceDocumentKey")));
            });
            persistJsonObjectEmail(message.correlationID(), envelope, payload, persistFuture);
        });
    }

    private void persistJsonObjectEmail(String correlationID, JsonObject envelope, JsonObject payload, Future<JsonObject> future) {
        String inReplyTo = envelope.getString("inReplyTo");
        String threadDocumentKey = envelope.getString("threadDocumentKey");
        String accountKey = envelope.getString("accountKey");
        String messageID = envelope.getString("messageID");
        Long timestamp = System.currentTimeMillis();

        LinkedList<String> correspondentsList = new LinkedList<>();
        if(envelope.containsKey("from")) {
            correspondentsList.add(envelope.getString("from").toLowerCase());
        }
        if(envelope.containsKey("toArray")) {
            envelope.getJsonArray("toArray").forEach(entry -> {
                correspondentsList.add(((String) entry).toLowerCase());
            });
        }
        if(envelope.containsKey("ccArray")) {
            envelope.getJsonArray("ccArray").forEach(entry -> {
                correspondentsList.add(((String) entry).toLowerCase());
            });
        }
        Collections.sort(correspondentsList);
        JsonArray correspondents = new JsonArray();
        String correspondentsString = "";

        for(int i=0;i<correspondentsList.size();i++) {
            correspondents.add(correspondentsList.get(i));
            correspondentsString += correspondentsList.get(i);
        }
        // hashcode is imperfect, collisions will arise
        Integer correspondentsHash = correspondentsString.hashCode();

        JsonObject messageDocument = new JsonObject()
                .put("envelope", envelope)
                .put("payload", payload)
                .put("messageID", messageID)
                .put("accountKey", accountKey);

        //TODO: save actual payload in storage (as files)

        if(envelope.containsKey("folderName") && envelope.containsKey("messageUID")) {
            messageDocument.put("imapID", envelope.getString("folderName") + "_" + envelope.getLong("messageUID"));
        }

        Future<JsonArray> insertFuture = Future.future();
        insertFuture.setHandler(insertResult -> {
            if(insertResult.failed()) {
                logger.info(correlationID, accountKey, 500, "Failed to get insert message due to a database error.", insertResult.cause());
                future.fail("Failed to persist message due to an unknown database error");
                return;
            }

            JsonArray resultArray = insertResult.result();
            JsonObject resultObject = resultArray.isEmpty()? null: insertResult.result().getJsonObject(0);

            if(resultObject == null || resultObject.containsKey("messageDocumentKey")) {
                logger.info(correlationID, accountKey, 500, "Failed to get insert message due to a database error: emtpy resultSet");
                future.fail("Failed to persist message due to an unknown database error");
                return;
            }

            String messageDocumentKey = resultObject.getString("messageKey");
            String returnedThreadDocumentKey = resultObject.getString("threadKey");
            String correspondenceDocumentKey = resultObject.getString("correspondenceDocumentKey");

            Future<JsonArray> correspondenceFuture = Future.future();
            correspondenceFuture.setHandler(correspondenceResult -> {
                if(correspondenceResult.failed()) {
                    String errorMessage = correspondenceResult.cause().getMessage();
                    if(errorMessage.contains("400") && errorMessage.contains("1226")) {
                        // likely to be a racing condition... try again once...
                        Future<JsonArray> retryFuture = Future.future();
                        retryFuture.setHandler(retryResult -> {
                           if(retryResult.failed()) {
                               logger.error(correlationID, accountKey, 500, "Failed to get insert message due to a database error when handling correspondence. Retry failed", correspondenceResult.cause());
                               future.fail("Failed to persist message due to an unknown database error related to correspondence");
                           }

                            future.complete(new JsonObject()
                                    .put("messageID", messageID)
                                    .put("messageDocumentKey", messageDocumentKey)
                                    .put("threadDocumentKey", returnedThreadDocumentKey)
                                    .put("correspondenceDocumentKey", correspondenceDocumentKey));
                        });
                        if(resultObject.containsKey("correspondenceDocumentWasExisting") && resultObject.getBoolean("correspondenceDocumentWasExisting")) {
                            // update correspondence linking
                            HashMap<String, Object> paramMap = new HashMap<>();
                            paramMap.put("correspondenceDocumentID",resultObject.getString("correspondenceDocumentID"));
                            paramMap.put("messageDocumentID", resultObject.getString("messageID"));
                            database.query(AQL_updateCorrespondenceLinks, paramMap, retryFuture);
                        }
                        else {
                            // create correspondence linking
                            HashMap<String, Object> paramMap = new HashMap<>();
                            paramMap.put("correspondenceDocumentKey",correspondenceDocumentKey);
                            paramMap.put("correspondenceDocumentID",resultObject.getString("correspondenceDocumentID"));
                            paramMap.put("correspondents", correspondents);
                            paramMap.put("accountID","accounts/" + accountKey);
                            paramMap.put("messageDocumentID",resultObject.getString("messageID"));
                            database.query(AQL_insertCorrespondenceLinks, paramMap, retryFuture);
                        }
                    }
                    else {
                        logger.error(correlationID, accountKey, 500, "Failed to get insert message due to a database error when handling correspondence.", correspondenceResult.cause());
                        future.fail("Failed to persist message due to an unknown database error related to correspondence");
                    }
                }
                else {
                    future.complete(new JsonObject()
                            .put("messageID", messageID)
                            .put("messageDocumentKey", messageDocumentKey)
                            .put("threadDocumentKey", returnedThreadDocumentKey)
                            .put("correspondenceDocumentKey", correspondenceDocumentKey));
                }
            });

            if(resultObject.containsKey("correspondenceDocumentWasExisting") && resultObject.getBoolean("correspondenceDocumentWasExisting")) {
                // update correspondence linking
                HashMap<String, Object> paramMap = new HashMap<>();
                paramMap.put("correspondenceDocumentID",resultObject.getString("correspondenceDocumentID"));
                paramMap.put("messageDocumentID", resultObject.getString("messageID"));
                database.query(AQL_updateCorrespondenceLinks, paramMap, correspondenceFuture);
            }
            else {
                // create correspondence linking
                HashMap<String, Object> paramMap = new HashMap<>();
                paramMap.put("correspondenceDocumentKey",correspondenceDocumentKey);
                paramMap.put("correspondenceDocumentID",resultObject.getString("correspondenceDocumentID"));
                paramMap.put("correspondents", correspondents);
                paramMap.put("accountID","accounts/" + accountKey);
                paramMap.put("messageDocumentID",resultObject.getString("messageID"));
                database.query(AQL_insertCorrespondenceLinks, paramMap, correspondenceFuture);

            }
        });

        // find the referenced message or duplicate first
        Future<JsonArray> checkFuture = Future.future();
        checkFuture.setHandler(checkResult -> {
           if(checkResult.failed()) {
               logger.info(correlationID, accountKey, 500, "Failed to get referenced message due to a database error.", checkResult.cause());
               future.fail("Failed to persist message due to an unknown database error");
               return;
           }

           JsonArray resultArray = checkResult.result();
           JsonObject resultObject = resultArray.isEmpty()? null: checkResult.result().getJsonObject(0);
           JsonArray thread = resultObject == null? null : resultObject.getJsonArray("thread");
           JsonArray duplicate = resultObject == null? null : resultObject.getJsonArray("duplicate");
           JsonArray reference = resultObject == null? null : resultObject.getJsonArray("reference");
           String identifiedThreadDocumentKey = null;

           // prefer reference over duplicate?
           if(thread != null && !thread.isEmpty()) {
               identifiedThreadDocumentKey = thread.getJsonObject(0).getString("threadDocumentKey");
           }
           if(identifiedThreadDocumentKey == null && duplicate != null && !duplicate.isEmpty()) {
               identifiedThreadDocumentKey = duplicate.getJsonObject(0).getString("threadDocumentKey");
           }
           if(identifiedThreadDocumentKey == null && reference != null && !reference.isEmpty()) {
               identifiedThreadDocumentKey = reference.getJsonObject(0).getString("threadDocumentKey");
           }

           if(identifiedThreadDocumentKey != null && !identifiedThreadDocumentKey.isEmpty()) {
               // insert and update thread
               HashMap<String, Object> paramMap = new HashMap<>();
               paramMap.put("accountKey", accountKey);
               paramMap.put("correspondentsHash", correspondentsHash);
               paramMap.put("timestamp", timestamp);
               paramMap.put("messageDocument", database.jsonObjectToParameterMap(messageDocument));
               paramMap.put("accountID", "accounts/" + accountKey);
               paramMap.put("threadDocumentKey", identifiedThreadDocumentKey);
               database.query(AQL_insertNewMessageUpdateThread, paramMap, insertFuture);
           }
           else {
               // insert with new thread
               HashMap<String, Object> paramMap = new HashMap<>();
               paramMap.put("accountKey", accountKey);
               paramMap.put("correspondentsHash", correspondentsHash);
               paramMap.put("timestamp", timestamp);
               paramMap.put("messageDocument", database.jsonObjectToParameterMap(messageDocument));
               paramMap.put("threadTitle", envelope.getString("subject"));
               paramMap.put("accountID", "accounts/" + accountKey);
               database.query(AQL_insertNewMessageAndThread, paramMap, insertFuture);
           }
        });

        HashMap<String, Object> paramMap = new HashMap<>();
        paramMap.put("accountKey", accountKey);
        paramMap.put("inReplyTo", inReplyTo == null? "null" : inReplyTo);
        paramMap.put("messageID", messageID == null? "null" : messageID);
        paramMap.put("threadDocumentKey", threadDocumentKey == null? "null" : threadDocumentKey);
        database.query(AQL_checkBeforeInsert, paramMap, checkFuture);
    }

    private void persistImapMessage(String correlationID, String accountKey, String folderName, Long messageUID, Message email, Future<JsonObject> future) {
        correlationID = correlationID == null? UUID.randomUUID().toString() : correlationID;

        try {
            String from = email.getFrom()[0].toString();
            String subject = email.getSubject();

            if(from == null || from.isEmpty()) {
                future.fail("email does not contain from entry!");
                return;
            }
            if(subject == null || subject.isEmpty()) {
                subject = "";
            }

            JsonObject envelope = new JsonObject()
                    .put("from", from)
                    .put("subject", subject)
                    .put("accountKey", accountKey);

            if(messageUID != null) {
                envelope.put("messageUID", messageUID);
            }
            if(folderName != null) {
                envelope.put("folderName", folderName);
            }

            String replyTo = email.getReplyTo() != null && email.getReplyTo().length > 0 ? email.getReplyTo()[0].toString() : null;
            if(replyTo != null) {
                envelope.put("replyTo", replyTo);
            }
            Long sentTimestamp = email.getSentDate().getTime();
            if(sentTimestamp != null) {
                envelope.put("sentTimestamp", sentTimestamp);
            }
            Long receivedTimestamp = email.getReceivedDate().getTime();
            if(receivedTimestamp != null) {
                envelope.put("receivedTimestamp", receivedTimestamp);
            }

            JsonObject payload = new JsonObject();

            JsonArray attachments = new JsonArray();
            JsonArray userFlags = new JsonArray();
            JsonArray systemFlags = new JsonArray();
            JsonObject headers = new JsonObject();
            JsonArray referencesArray = new JsonArray();
            JsonArray toArray = new JsonArray();
            JsonArray ccArray = new JsonArray();
            JsonArray bccArray = new JsonArray();
            String messageID = null;

            Address[] addresses = email.getRecipients(Message.RecipientType.TO);
            if(addresses != null) {
                for (Address address : addresses) {
                    String currentAddress = address.toString().trim();
                    String type = address.getType();

                    if (!"rfc822".equalsIgnoreCase(type)) {
                        logger.warn(correlationID, accountKey, 501, "Recipient address type '" + type + "' unknown. Correspondence attribution might by incorrect.");
                    }
                    toArray.add(currentAddress);
                }
                envelope.put("toArray", toArray);
            }
            addresses = email.getRecipients(Message.RecipientType.CC);
            if(addresses != null) {
                for (Address address : addresses) {
                    String currentAddress = address.toString().trim();
                    String type = address.getType();

                    if (!"rfc822".equalsIgnoreCase(type)) {
                        logger.warn(correlationID, accountKey, 501, "Recipient address type '" + type + "' unknown. Correspondence attribution might by incorrect.");
                    }
                    ccArray.add(currentAddress);
                }
                envelope.put("ccArray", ccArray);
            }
            addresses = email.getRecipients(Message.RecipientType.BCC);
            if(addresses != null) {
                for (Address address : addresses) {
                    String currentAddress = address.toString().trim();
                    String type = address.getType();

                    if (!"rfc822".equalsIgnoreCase(type)) {
                        logger.warn(correlationID, accountKey, 501, "Recipient address type '" + type + "' unknown. Correspondence attribution might by incorrect.");
                    }
                    bccArray.add(currentAddress);
                }
                envelope.put("bccArray", bccArray);
            }

            if (email.getFlags() != null) {
                for (String userFlag : email.getFlags().getUserFlags()) {
                    userFlags.add(userFlag);
                }
                if (!userFlags.isEmpty()) {
                    envelope.put("userFlags", userFlags);
                }

                for (Flags.Flag flag : email.getFlags().getSystemFlags()) {
                    systemFlags.add(flag.toString());
                }
                if (!systemFlags.isEmpty()) {
                    envelope.put("systemFlags", systemFlags);
                }
            }

            Enumeration<Header> headerEnumeration = email.getAllHeaders();
            while(headerEnumeration.hasMoreElements()) {
                Header header = headerEnumeration.nextElement();
                headers.put(header.getName(), header.getValue());
            }
            if(!headers.isEmpty()) {
                envelope.put("headers", headers);
            }

            String[] messageIDArray = email.getHeader("Message-Id");
            String[] inReplyToArray = email.getHeader("In-Reply-To");
            String[] references = email.getHeader("References");

            if(messageIDArray != null && messageIDArray.length > 0) {
                messageID = messageIDArray[0];

                if(messageID == null || messageID.isEmpty()) {
                    logger.warn(correlationID, 400, "Received mail message has no id. Setting fake id...");
                    messageID = UUID.randomUUID().toString();
                }
                else {
                    messageID = messageID.trim();
                }

                if(messageID.startsWith("<") && messageID.endsWith(">")) {
                    messageID = messageID.substring(1, messageID.length()-1);
                }
                envelope.put("messageID", messageID);
            }

            String inReplyTo = null;
            if(inReplyToArray != null && inReplyToArray.length > 0) {
                inReplyTo = inReplyToArray[0];

                if(inReplyTo != null && inReplyTo.startsWith("<") && inReplyTo.endsWith(">")) {
                    inReplyTo = inReplyTo.substring(1, inReplyTo.length()-1);
                }
                envelope.put("inReplyTo", inReplyTo);
            }

            if(references != null && references.length > 0) {
                for(int i=0;i<references.length;i++) {
                    String reference = references[i];

                    if(reference != null && !reference.isEmpty()) {
                        reference = reference.trim();
                        if(reference.startsWith("<") && reference.endsWith(">")) {
                            reference  = reference.substring(1, reference.length()-1);
                        }
                        referencesArray.add(reference);
                    }
                }
            }
            if(!referencesArray.isEmpty()) {
                envelope.put("references", referencesArray);

                if(inReplyTo == null) {
                    inReplyTo = referencesArray.getString(referencesArray.size() -1);
                    envelope.put("inReplyTo", inReplyTo);
                }
            }

            String contentType = email.getContentType();
            envelope.put("contentType", contentType);


            if (contentType.contains("multipart")) {
                Multipart multiPart = (Multipart) email.getContent();

                for (int i = 0; i < multiPart.getCount(); i++) {
                    MimeBodyPart mimeBodyPart = (MimeBodyPart) multiPart.getBodyPart(i);
                    if (Part.ATTACHMENT.equalsIgnoreCase(mimeBodyPart.getDisposition())) {
                        logger.warn(correlationID, 501, "Attachments not supported by mail retrieval yet! Encountered: " + mimeBodyPart.getFileName() + " (" + mimeBodyPart.getContentType() + ", " + mimeBodyPart.getSize() + " bytes)");

                        JsonObject attachment = new JsonObject()
                                .put("name", mimeBodyPart.getFileName())
                                .put("size", mimeBodyPart.getSize())
                                .put("contentType", mimeBodyPart.getContentType())
                                .put("id", mimeBodyPart.getContentID())
                                .put("md5", mimeBodyPart.getContentMD5())
                                .put("description", mimeBodyPart.getDescription())
                                .put("encoding", mimeBodyPart.getEncoding());

                        // TODO: persist actual attachment to restricted storage

                        attachments.add(attachment);
                    }
                    else if (mimeBodyPart.getContentType().contains("text/html")) {
                        Object content = mimeBodyPart.getContent();
                        if (content != null) {
                            payload.put("html", content.toString().trim());
                        }
                    }
                    else if (mimeBodyPart.getContentType().contains("text/plain")) {
                        Object content = mimeBodyPart.getContent();
                        if (content != null) {
                            payload.put("text", content.toString().trim());
                        }
                    }
                }
            }
            else if (contentType.contains("text/plain")) {
                Object content = email.getContent();
                if (content != null) {
                    payload.put("text", content.toString().trim());
                    payload.put("html", "");
                }
            }
            else if (contentType.contains("text/html")) {
                Object content = email.getContent();
                if (content != null) {
                    payload.put("html", content.toString().trim());
                    payload.put("text", "");
                }
            }
            if(!attachments.isEmpty()) {
                payload.put("attachments", attachments);
            }

            persistJsonObjectEmail(correlationID, envelope, payload, future);
        }
        catch (MessagingException e) {
            logger.error(correlationID, 500, "Unknown messaging exception when receiving mail", e);
            future.fail("Unknown messaging exception when receiving mail");
        }
        catch(Exception e) {
            logger.error(correlationID, 500, "Unknown exception when receiving mail", e);
            future.fail("Unknown exception when receiving mail");
        }
    }

    private void prepareMonitoringOfImapFolder(String correlationID, String accountKey, String folderName, JsonArray notificationAddresses, Future<Boolean> future) {
        String cid = correlationID == null ? UUID.randomUUID().toString() : correlationID;

        JsonObject serverSettings = accountConfigMap.containsKey(accountKey) ? accountConfigMap.get(accountKey).getJsonObject("serverSettings") : null;
        if (serverSettings == null || serverSettings.isEmpty()) {
            logger.warn(cid, 404, "Server settings with " + accountKey + " accountKey not found/setup!");
            future.fail("Server config with " + accountKey + " + accountKey not found/setup!");
            return;
        }

        if (folderName == null || folderName.isEmpty()) {
            logger.warn(cid, 400, "'folderName' must not be empty");
            future.fail("Must provide non-empty 'folderName'");
            return;
        }

        JsonObject monitoringSettings = accountConfigMap.get(accountKey).getJsonObject("monitoringSettings");

        if(notificationAddresses == null) {
            notificationAddresses = new JsonArray();
        }

        String monitorKey = folderName + "@" + accountKey;
        if (monitorFolderMap.containsKey(monitorKey)) {
            logger.info(cid, 204, "Monitor already setup for " + accountKey + ":" + folderName);
            future.complete(true);
        }
        JsonObject monitorConfig = new JsonObject()
                .put("accountKey", accountKey)
                .put("folderName", folderName)
                .put("isIdleUsageEnabled", monitoringSettings.getBoolean("isIdleUsageEnabled"))
                .put("forceReconnectTimerInMinutes", monitoringSettings.getLong("forceReconnectTimerInMinutes"))
                .put("fallbackPullInMS", monitoringSettings.getLong("fallbackPullInMS"))
                .put("errorRetryPauseInMS", monitoringSettings.getLong("errorRetryPauseInMS"))
                .put("isBusy", true)
                .put("canaryTimestamp", System.currentTimeMillis())
                .put("isShutdownRequested", false)
                .put("notificationAddresses", notificationAddresses);
        monitorConfigMap.put(monitorKey, monitorConfig);

        // prepare the monitor
        try {
            // connect to IMAP server
            Properties config = new Properties();
            if (serverSettings.getBoolean("isStartTLSRequired")) {
                config.setProperty("mail.store.protocol", "imaps");
            } else {
                config.setProperty("mail.store.protocol", "imap");
                logger.security(cid, 900, "Mail service " + serverSettings.getString("imapHost") + " transmits CREDENTIALS UNENCRYPTED - except in local testing context THIS IS A SEVERE SECURITY RISK!");
            }

            Session session = Session.getDefaultInstance(config);
            Store monitoringStore = null;

            if (serverSettings.getBoolean("isStartTLSRequired")) {
                monitoringStore = session.getStore("imaps");
                future.fail("IMAPS for monitoring not supported yet!");
                logger.fatal(cid, "IMAPS for monitoring IMAP folder not implemented yet!");
                return;
            } else {
                monitoringStore = session.getStore("imap");
            }

            monitoringStore.connect(serverSettings.getString("imapHost"), serverSettings.getInteger("imapPort"), serverSettings.getString("user"), serverSettings.getString("password"));
            IMAPFolder monitoringFolder = (IMAPFolder) monitoringStore.getFolder(folderName);

            if (monitoringFolder == null || !monitoringFolder.exists()) {
                future.fail("Failed to find IMAP folder " + folderName + " on server for monitoring! Please check spelling in config");
                logger.warn(cid, 400, "Failed to find IMAP folder " + folderName + " on server for monitoring! Please check spelling in config");
                return;
            }
            if (!monitoringFolder.isOpen()) {
                monitoringFolder.open(Folder.READ_ONLY);
            }
            if (!monitoringFolder.isOpen()) {
                future.fail("Failed to open monitoringFolder " + folderName + " for monitoring (read only)! Please check access rights");
                logger.error(cid, 401, "Failed to open monitoringFolder " + folderName + " for monitoring (read only)! Please check access rights");
                return;
            }

            FetchProfile fetchProfile = new FetchProfile();
            fetchProfile.add(FetchProfile.Item.ENVELOPE);
            fetchProfile.add(FetchProfile.Item.FLAGS);
            fetchProfile.add(FetchProfile.Item.CONTENT_INFO);
            fetchProfile.add(FetchProfile.Item.SIZE);

            // register monitoring folder
            monitorFolderMap.put(monitorKey, monitoringFolder);

            // set handler for new messages
            monitoringFolder.addMessageCountListener(new MessageCountAdapter() {
                @Override
                public void messagesAdded(MessageCountEvent e) {
                    vertx.executeBlocking(messagesFuture -> {
                        try {
                            // first log that operation is in progress/avoid premature folder closing...
                            monitorConfig.put("isBusy", true);
                            monitorConfig.put("canaryTimestamp", System.currentTimeMillis());

                            JsonArray notificationAddresses = monitorConfig.getJsonArray("notificationAddresses");
                            Message[] messages = e.getMessages();
                            String folderName = monitoringFolder.getName();


                            // persist slowly in chronological order and sequentially in order to minimize risk of racing conditions/broken threads
                            int messageCount = messages.length;
                            Long[] messageUIDs = new Long[messageCount];
                            Future<JsonObject>[] futures = new Future[messageCount];
                            JsonArray failures = new JsonArray();
                            JsonArray successes = new JsonArray();

                            messageUIDs[0] = monitoringFolder.getUID(messages[0]);
                            for (int i = 1; i < messageCount; i++) {
                                int nextPosition = i;
                                messageUIDs[nextPosition] = monitoringFolder.getUID(messages[nextPosition]);

                                Future<JsonObject> persistenceFuture = Future.future();
                                futures[nextPosition-1] = persistenceFuture;
                                persistenceFuture.setHandler(persistenceResult -> {
                                    Long previousMessageID = messageUIDs[nextPosition-1];

                                    if(persistenceResult.failed()) {
                                        failures.add(previousMessageID);
                                        logger.warn(cid, accountKey, 500, "Failed to persist message with imap id " + previousMessageID + " when retrieving new mails for folder " + folderName, persistenceResult.cause());
                                    }
                                    else {
                                        successes.add(previousMessageID);

                                        JsonObject msg = persistenceResult.result();
                                        if (notificationAddresses != null && !notificationAddresses.isEmpty()) {
                                            for (int a = 0; a < notificationAddresses.size(); a++) {
                                                JsonObject address = notificationAddresses.getJsonObject(a);
                                                String domain = address.getString("domain");
                                                String version = address.getString("version");
                                                String type = address.getString("type");
                                                send(domain, version, type, serviceName, cid, msg);
                                            }
                                        }
                                    }

                                    // persist the next message
                                    persistImapMessage(cid, accountKey, folderName, messageUIDs[nextPosition], messages[nextPosition], futures[nextPosition]);
                                });
                            }

                            Future<JsonObject> endFuture = Future.future();
                            futures[messageCount -1] = endFuture;
                            endFuture.setHandler(endResult -> {
                                Long previousMessageID = messageUIDs[messageCount -1];
                                if(endResult.failed()) {
                                    failures.add(previousMessageID);
                                    logger.warn(cid, accountKey, 500, "Failed to persist message with imap id " + previousMessageID + "during monitoring of IMAP folder: " + folderName, endResult.cause());
                                }
                                else {
                                    successes.add(previousMessageID);

                                    JsonObject msg = endResult.result();
                                    if (notificationAddresses != null && !notificationAddresses.isEmpty()) {
                                        for (int a = 0; a < notificationAddresses.size(); a++) {
                                            JsonObject address = notificationAddresses.getJsonObject(a);
                                            String domain = address.getString("domain");
                                            String version = address.getString("version");
                                            String type = address.getString("type");
                                            send(domain, version, type, serviceName, cid, msg);
                                        }
                                    }
                                }

                                // check if a shutdown/reconnect is requested
                                if (monitorConfig.getBoolean("isShutdownRequested")) {
                                    logger.info(cid, 201, "Shutting down imap folder monitor " + monitorKey);

                                    Long periodicID = monitorConfig.getLong("periodicID");
                                    monitorFolderMap.remove(monitorKey);
                                    monitorConfigMap.remove(monitorKey);

                                    try {
                                        if (monitoringFolder.isOpen()) {
                                            monitoringFolder.close();
                                        }
                                        monitoringFolder.getStore().close();
                                    } catch (MessagingException ex) {
                                        logger.error(cid, "Shutting down imap folder monitor " + monitorKey + " caused an messaging exception", ex);
                                    }
                                    if (periodicID != null) {
                                        vertx.cancelTimer(periodicID);
                                    }
                                }

                                messagesFuture.complete(new JsonObject()
                                        .put("successes", successes)
                                        .put("failures", failures)
                                );
                            });

                            // fetch the mail meta data from the server at once (might be a huge amount of data!)
                            monitoringFolder.fetch(messages, fetchProfile);

                            // start persisting
                            persistImapMessage(cid, accountKey, folderName, messageUIDs[0], messages[0], futures[0]);
                        }
                        catch (MessagingException e1) {
                            logger.warn(cid, accountKey, 500, "Failed to retrieve new mails for IMAP folder " + folderName, e1);
                            messagesFuture.fail("Failed to retrieve new mails for IMAP folder " + folderName);
                        }
                    }, messagesResult -> {
                        monitorConfig.put("isBusy", false);

                        if(messagesResult.failed()) {
                            logger.fatal(cid, accountKey, 500, "Failed to retrieve new mails for IMAP folder " + folderName, messagesResult.cause());
                            return;
                        }

                        JsonObject result = (JsonObject) messagesResult.result();
                        if(result.containsKey("failures") && !result.getJsonArray("failures").isEmpty()) {
                            logger.error(cid, accountKey, 500,"Retrieving of at least some of the new messages of IMAP folder " + folderName + " failed. Their UIDs are:" + result.getJsonArray("failures").encodePrettily());
                        }
                    });
                }
            });



            // initial population of database with existing messages
            // run in the background, already signal successful preparation. However, via monitorConfig.put("isBusy", false); pulling is disabled until this task is finished
            vertx.executeBlocking(populationFuture -> {
                try {
                    //TODO: break down into smaller consecutive processing chunks (also allows for stopping as soon as messages in the current chunk are already known)

                    // persist in a slow, chronological, sequential process in order to minimize risk of running conditions/separated threads
                    int initialMessageCount = monitoringFolder.getMessageCount();

                    JsonArray imapIDs = new JsonArray();
                    HashMap<Integer, Message> messageNumberMessageMap = new HashMap();
                    HashMap<Integer, Long> messageNumberUIDMap = new HashMap<>();
                    HashMap<String, Integer> imapIDMessageNumberMap = new HashMap<>();
                    
                    for(int i=1;i<=initialMessageCount;i++) {
                        Message message = monitoringFolder.getMessage(i);
                        Long messageUID = monitoringFolder.getUID(message);
                        Integer messageNumber = message.getMessageNumber();

                        String imapID = folderName + "_" + messageUID;
                        imapIDs.add(folderName + "_" + messageUID);

                        imapIDMessageNumberMap.put(imapID, messageNumber);
                        messageNumberUIDMap.put(messageNumber, messageUID);
                        messageNumberMessageMap.put(messageNumber, message);
                    }

                    if(imapIDs.size() > 0) {
                        Future<JsonArray> identifyFuture = Future.future();
                        identifyFuture.setHandler(identifyResult -> {
                            if (identifyResult.failed()) {
                                logger.warn(cid, accountKey,500, "Setting up imap(s) folder monitor for " + folderName + " at " + serverSettings.getString("imapHost") + " incomplete - identifying missing mails failed", identifyResult.cause());
                                populationFuture.fail("Setting up imap(s) folder monitor for " + folderName + " at " + serverSettings.getString("imapHost") + " incomplete - identifying missing mails failed");
                                return;
                            }
                            JsonArray resultArray = identifyResult.result();
                            if (!resultArray.isEmpty()) {

                                JsonArray missingImapIDs = resultArray.getJsonObject(0).getJsonArray("missingImapIDs");
                                LinkedList<Integer> missingMessageNumbersList = new LinkedList<>();

                                int missingMessagesCount = missingImapIDs.size();
                                for(int i=0;i<missingMessagesCount;i++) {
                                    missingMessageNumbersList.add(imapIDMessageNumberMap.remove(missingImapIDs.getString(i)));
                                }
                                Collections.sort(missingMessageNumbersList);

                                // trying to let garbage collector to start as early as possible
                                imapIDMessageNumberMap.clear();
                                missingImapIDs.clear();

                                JsonArray failures = new JsonArray();
                                JsonArray successes = new JsonArray();

                                Future<JsonObject>[] futures = new Future[missingMessagesCount];
                                Message[] messages = new Message[missingMessagesCount];
                                Long[] messageUIDs = new Long[missingMessagesCount];

                                // add the messages which will not be covered by the loop
                                messages[0] = messageNumberMessageMap.remove(missingMessageNumbersList.get(0));
                                messageUIDs[0] = messageNumberUIDMap.remove(missingMessageNumbersList.get(0));
                                // leaving out the first message, note that futures are added at currentPosition - 1
                                for (int i = 1; i < missingMessagesCount; i++) {
                                    int nextPosition = i;
                                    int messageNumber = missingMessageNumbersList.get(nextPosition);
                                    messages[nextPosition] = messageNumberMessageMap.remove(messageNumber);
                                    messageUIDs[nextPosition] = messageNumberUIDMap.remove(messageNumber);

                                    Future<JsonObject> persistenceFuture = Future.future();
                                    futures[nextPosition-1] = persistenceFuture;
                                    persistenceFuture.setHandler(persistenceResult -> {
                                        Long previousMessageID = messageUIDs[nextPosition-1];

                                        if(persistenceResult.failed()) {
                                            failures.add(previousMessageID);
                                            logger.warn(cid, accountKey, 500, "Failed to persist message with imap id " + previousMessageID + "during database population with existing messages!", persistenceResult.cause());
                                        }
                                        else {
                                            successes.add(previousMessageID);
                                        }

                                        persistImapMessage(cid, accountKey, folderName, messageUIDs[nextPosition], messages[nextPosition], futures[nextPosition]);
                                    });
                                }

                                // trying to let garbage collector to start as early as possible
                                messageNumberMessageMap.clear();
                                messageNumberUIDMap.clear();


                                // add the end future which is called after all messages have been dealt with
                                Future<JsonObject> endFuture = Future.future();
                                futures[missingMessagesCount -1] = endFuture;
                                endFuture.setHandler(endResult -> {
                                    Long previousMessageID = messageUIDs[missingMessagesCount -1];
                                    if(endResult.failed()) {
                                        failures.add(previousMessageID);
                                        logger.warn(cid, accountKey, 500, "Failed to persist message with imap id " + previousMessageID + "during database population with existing messages!", endResult.cause());
                                    }
                                    else {
                                        successes.add(previousMessageID);
                                    }

                                    populationFuture.complete(new JsonObject()
                                            .put("successes", successes)
                                            .put("failures", failures)
                                        );
                                });

                                // fetch the mail meta data from the server at once
                                try {
                                    monitoringFolder.fetch(messages, fetchProfile);
                                } catch (MessagingException e1) {
                                    logger.warn(cid, accountKey, 500, "Fetching meta data of new mails failed!", e1);
                                }

                                // start persisting
                                persistImapMessage(cid, accountKey, folderName, messageUIDs[0], messages[0], futures[0]);
                            }
                        });
                        HashMap<String, Object> paramMap = new HashMap<>();
                        paramMap.put("accountKey", accountKey);
                        paramMap.put("imapIDs", imapIDs);
                        database.query(AQL_identifyMissingImapIDs, paramMap, identifyFuture);
                    }
                    else {
                        populationFuture.complete(new JsonObject()
                                .put("successes", new JsonArray())
                                .put("failures", new JsonArray()));
                    }
                } catch (MessagingException ex) {
                    logger.warn(cid, accountKey, 500, "Exception when processing mail of monitored imap folder (initial email retrieval)", ex);
                    populationFuture.fail("Exception when processing mail of monitored imap folder (initial email retrieval): " + ex.getMessage());
                }
            }, populationResult -> {
                // allow for pulling
                monitorConfig.put("isBusy", false);

                if(populationResult.failed()) {
                    logger.fatal(cid, accountKey, 500, "Failed to retrieve initial mails of IMAP folder " + folderName, populationResult.cause());
                }
                JsonObject result = (JsonObject) populationResult.result();

                if(result.containsKey("failures") && !result.getJsonArray("failures").isEmpty()) {
                    logger.error(cid, accountKey, 500,"Fetching at least some of the messages of IMAP folder " + folderName + " failed. Their UIDs are:" + result.getJsonArray("failures").encodePrettily());
                }
            });

            future.complete(true);
        } catch (NoSuchProviderException e) {
            logger.error(cid, 400, "Failed to set up imap(s) folder monitor for " + folderName + " at " + serverSettings.getString("imapHost") + " - no such service (imap/imaps) provider", e);
            future.fail("Failed to set up imap(s) folder monitor for " + folderName + " at " + serverSettings.getString("imapHost") + " - no such service (imap/imaps) provider");
            return;
        } catch (MessagingException e) {
            logger.error(cid, 500, "Failed to set up imap(s) folder monitor for " + folderName + " at " + serverSettings.getString("imapHost") + " - error while connecting", e);
            future.fail("Failed to set up imap(s) folder monitor for " + folderName + " at " + serverSettings.getString("imapHost") + " - error while connecting");
            return;
        } catch (Exception e) {
            logger.error(cid, 500, "Failed to set up imap(s) folder monitor for " + folderName + " at " + serverSettings.getString("imapHost") + " - unknown error", e);
            future.fail("Failed to set up imap(s) folder monitor for " + folderName + " at " + serverSettings.getString("imapHost") + " - unknown error");
            return;
        }
    }

    private void startMonitoringOfImapFolder(String correlationID, String monitorKey, Future<Boolean> future) {
        String cid = correlationID == null? UUID.randomUUID().toString() : correlationID;
        JsonObject monitorConfig = monitorConfigMap.get(monitorKey);
        IMAPFolder imapFolder = monitorFolderMap.get(monitorKey);

        if(monitorConfig == null || monitorConfig.isEmpty()) {
            logger.error(cid, 500, "'monitorConfig' with key " + monitorKey + " not found or empty!");
            future.fail("'monitorConfig' with key " + monitorKey + " not found or empty!");
            return;
        }
        if(imapFolder == null) {
            logger.error(cid, 500, "'imapFolder' with key " + monitorKey + " not found!");
            future.fail("'imapFolder' with key " + monitorKey + " not found!");
            return;
        }

        // make sure there is not already a monitoring job. If there is - cancel it
        Long periodicID = monitorConfig.getLong("periodicID");
        if(periodicID != null) {
            vertx.cancelTimer(periodicID);
        }

        // start the periodic check
        periodicID = vertx.setPeriodic(monitorConfig.getLong("fallbackPullInMS"), handler -> {
            if(monitorConfig.containsKey("isBusy") && monitorConfig.getBoolean("isBusy")) {
                // skip, as messages are still being retrieved
                return;
            }

            try {
                // triggers the message handler registered earlier
                imapFolder.getMessageCount();
            }
            catch (FolderClosedException e) {
                Long currentPeriodicID = monitorConfig.getLong("periodicID");
                if(currentPeriodicID != null) {
                    vertx.cancelTimer(currentPeriodicID);
                }

                if(monitorFolderMap.containsKey(monitorKey)) {
                    // this is not a intended shutdown - restart
                    logger.warn(cid, 500, "Exception while monitoring " + monitorKey + " via pull. Retrying in " + monitorConfig.getLong("errorRetryPauseInMS") + "ms");
                    monitorFolderMap.remove(monitorKey);
                    currentPeriodicID = vertx.setTimer(monitorConfig.getLong("errorRetryPauseInMS"), retryHandler -> {
                        prepareMonitoringOfImapFolder(cid, monitorConfig.getString("accountKey"), monitorConfig.getString("folderName"), monitorConfig.getJsonArray("notificationAddresses"), Future.future());
                    });
                    monitorConfig.put("periodicID", currentPeriodicID);
                }
            }
            catch (Exception e) {
                Long currentPeriodicID = monitorConfig.getLong("periodicID");
                if(currentPeriodicID != null) {
                    vertx.cancelTimer(currentPeriodicID);
                }

                if(monitorFolderMap.containsKey(monitorKey)) {
                    // this is not a intended shutdown - restart
                    logger.error(cid, 500, "Exception while monitoring " + monitorKey + " via pull. Retrying in " + monitorConfig.getLong("errorRetryPauseInMS") + "ms");
                    monitorFolderMap.remove(monitorKey);
                    currentPeriodicID = vertx.setTimer(monitorConfig.getLong("errorRetryPauseInMS"), retryHandler -> {
                        prepareMonitoringOfImapFolder(cid, monitorConfig.getString("accountKey"), monitorConfig.getString("folderName"), monitorConfig.getJsonArray("notificationAddresses"), Future.future());
                    });
                    monitorConfig.put("periodicID", currentPeriodicID);
                }
            }
        });
        monitorConfig.put("periodicID", periodicID);
        future.complete(true);
    }

    private void verifyServerCredentials(String correlationID, JsonObject serverSettings, boolean isSmtpCheckRequired, Future<Boolean> future) {
        correlationID = correlationID == null ? UUID.randomUUID().toString() : correlationID;

        if(isSmtpCheckRequired) {
            future.fail("SMTP check not implemented yet!");
            return;
        }

        if(serverSettings == null || serverSettings.isEmpty()) {
            future.fail("Must provide non empty server settings!");
            return;
        }
        if(isSmtpCheckRequired && !serverSettings.containsKey("smtpHost")) {
            future.fail("Must provide non empty serverSettings.smtpHost!");
            return;
        }
        if(isSmtpCheckRequired && !serverSettings.containsKey("smtpPort")) {
            future.fail("Must provide non empty serverSettings.smtpPort!");
            return;
        }
        if(!serverSettings.containsKey("imapHost")) {
            future.fail("Must provide non empty serverSettings.imapHost!");
            return;
        }
        if(!serverSettings.containsKey("imapPort")) {
            future.fail("Must provide non empty serverSettings.imapPort!");
            return;
        }
        if(!serverSettings.containsKey("user")) {
            future.fail("Must provide non empty serverSettings.user!");
            return;
        }
        if(!serverSettings.containsKey("password")) {
            future.fail("Must provide non empty serverSettings.password!");
            return;
        }
        if(!serverSettings.containsKey("isStartTLSRequired")) {
            serverSettings.put("isStartTLSRequired", true);
        }
        if(!serverSettings.containsKey("isServerCertCheckRequired")) {
            serverSettings.put("isServerCertCheckRequired", true);
        }

        Properties config = new Properties();
        if (serverSettings.getBoolean("isStartTLSRequired")) {
            config.setProperty("mail.store.protocol", "imaps");
        }
        else {
            config.setProperty("mail.store.protocol", "imap");
            logger.security(correlationID, 900, "Mail service for " + serverSettings.getString("imapHost") + " transmits CREDENTIALS UNENCRYPTED - except in local testing context THIS IS A SEVERE SECURITY RISK!");
        }

        try {
            Session session = Session.getDefaultInstance(config);
            Store store = null;

            if (serverSettings.getBoolean("isStartTLSRequired")) {
                //monitoringStore = session.getStore("imaps");
                future.fail("IMAPS not supported yet");
                return;
            }
            else {
                store = session.getStore("imap");
            }

            store.connect(serverSettings.getString("imapHost"), serverSettings.getInteger("imapPort"), serverSettings.getString("user"), serverSettings.getString("password"));
            store.close();
        } catch (NoSuchProviderException e) {
            future.fail("Requested IMAP(s) provider is not available: " + e.getMessage());
            return;
        } catch (MessagingException e) {
            future.fail("Messaging with server failed: " + e.getMessage());
            return;
        } catch (Exception e) {
            future.fail("Unknown error: " + e.getMessage());
            return;
        }

        future.complete(true);
    }

    private void ensureAccountDocumentEntry(String accountKey, Future<Void> future) {
        if(accountKey == null || accountKey.isEmpty()) {
            future.fail("Must provide non-empty accountKey!");
            return;
        }
        Long timestamp = System.currentTimeMillis();

        Future<JsonArray> ensureFuture = Future.future();
        ensureFuture.setHandler(ensureResult -> {
            if (ensureResult.failed()) {
                future.fail(ensureResult.cause());
                return;
            }
            JsonArray result = ensureResult.result();
            if (result == null || result.isEmpty()) {
                future.fail("Failed for an unknown reason! Empty result when upserting " + accountKey);
                return;
            }

            JsonArray lastAccountMessage = result.getJsonObject(0).getJsonArray("lastAccountMessage");
            if(lastAccountMessage.isEmpty()) {
                Future<JsonArray> ensureLAMFuture = Future.future();
                ensureLAMFuture.setHandler(ensureLAMResult -> {
                    if (ensureLAMResult.failed()) {
                        future.fail(ensureLAMResult.cause());
                        return;
                    }
                    JsonArray lamResult = ensureLAMResult.result();
                    if (lamResult == null || lamResult.isEmpty()) {
                        future.fail("Failed for an unknown reason! Empty result when inserting empty first message for " + accountKey);
                        return;
                    }
                    future.complete();
                });

                HashMap<String, Object> paramMap = new HashMap<>();
                paramMap.put("accountKey", accountKey);
                paramMap.put("accountID", "accounts/" + accountKey);
                database.query(AQL_ensureLastAccountMessageEntry, paramMap, ensureLAMFuture);
            }
            else {
                future.complete();
            }
        });
        HashMap<String, Object> paramMap = new HashMap<>();
        paramMap.put("accountKey", accountKey);
        paramMap.put("timestamp",timestamp);
        database.query(AQL_ensureAccountDocumentEntry, paramMap, ensureFuture);
    }

    private void setupAccount(JsonObject accountSettings, Future<Object> future) {
        String accountKey = accountSettings.getString("accountKey");

        if(accountKey == null || accountKey.isEmpty()) {
            future.fail("Must provide non-empty account key!");
            return;
        }

        // TODO: config might be different - need to warn invoking instance!
        if(accountConfigMap.containsKey(accountKey)) {
            future.complete();
            return;
        }

        // make sure all required variables are set
        if(!accountSettings.containsKey("isSendingEnabled")) {
            logger.settings(accountKey + ".isSendingEnabled", false);
            accountSettings.put("isSendingEnabled", false);
        }
        JsonObject sendingSettings = accountSettings.getJsonObject("sendingSettings");
        if(sendingSettings == null) {
            sendingSettings = new JsonObject();
            logger.settings(accountKey + ".sendingSettings", 900, "sendingSettings not found! Default values might not be applicable!");
            accountSettings.put("sendingSettings", sendingSettings);
        }
        if(!sendingSettings.containsKey("whitelistByServiceOrigin")) {
            logger.settings(accountKey + ".sendingSettings.whitelistByServiceOrigin", "[]");
            sendingSettings.put("whitelistByServiceOrigin", new JsonArray());
        }
        if(!sendingSettings.containsKey("emailWhitelistPattern")) {
            logger.settings(accountKey + ".sendingSettings.emailWhitelistPattern", "^[\\w-\\+]+(\\.[\\w]+)*@[\\w-]+(\\.[\\w]+)*(\\.[a-z]{2,})$");
            sendingSettings.put("emailWhitelistPattern", "^[\\w-\\+]+(\\.[\\w]+)*@[\\w-]+(\\.[\\w]+)*(\\.[a-z]{2,})$");
        }
        if(!sendingSettings.containsKey("ehloHostname")) {
            logger.settings(accountKey + ".sendingSettings.ehloHostname", "localhost");
            sendingSettings.put("ehloHostname", "localhost");
        }
        if(!sendingSettings.containsKey("isAllowRcptErrors")) {
            logger.settings(accountKey + ".sendingSettings.isAllowRcptErrors", false);
            sendingSettings.put("isAllowRcptErrors", false);
        }
        if(!accountSettings.containsKey("isMonitoringEnabled")) {
            logger.settings(accountKey + ".isMonitoringEnabled", false);
            accountSettings.put("isMonitoringEnabled", false);
        }
        JsonObject monitoringSettings = accountSettings.getJsonObject("monitoringSettings");
        if(monitoringSettings == null) {
            monitoringSettings = new JsonObject();
            logger.settings(accountKey + ".monitoringSettings", 900, "monitoringSettings not found! Default values might not be applicable!");
            accountSettings.put("monitoringSettings", monitoringSettings);
        }
        if(!monitoringSettings.containsKey("whitelistByServiceOrigin")) {
            logger.settings(accountKey + ".monitoringSettings.whitelistByServiceOrigin", "[]");
            monitoringSettings.put("whitelistByServiceOrigin", new JsonArray());
        }
        if(!monitoringSettings.containsKey("folderName")) {
            logger.settings(accountKey + ".monitoringSettings.folderName", "Inbox");
            monitoringSettings.put("folderName", "Inbox");
        }
        if(!monitoringSettings.containsKey("notificationAddresses")) {
            logger.settings(accountKey + ".monitoringSettings.notificationAddresses", "[]");
            monitoringSettings.put("folderName", new JsonArray());
        }
        if(!monitoringSettings.containsKey("isIdleUsageEnabled")) {
            logger.settings(accountKey + ".monitoringSettings.isIdleUsageEnabled", false);
            monitoringSettings.put("isIdleUsageEnabled", false);
        }
        if(!monitoringSettings.containsKey("forceReconnectTimerInMinutes")) {
            logger.settings(accountKey + ".monitoringSettings.forceReconnectTimerInMinutes", 1);
            monitoringSettings.put("forceReconnectTimerInMinutes", 1);
        }
        if(!monitoringSettings.containsKey("fallbackPullInMS")) {
            logger.settings(accountKey + ".monitoringSettings.fallbackPullInMS", 2000);
            monitoringSettings.put("fallbackPullInMS", 2000);
        }
        if(!monitoringSettings.containsKey("errorRetryPauseInMS")) {
            logger.settings(accountKey + ".monitoringSettings.errorRetryPauseInMS", 5000);
            monitoringSettings.put("errorRetryPauseInMS", 5000);
        }
        JsonObject serverSettings = accountSettings.getJsonObject("serverSettings");
        if(serverSettings == null) {
            serverSettings = new JsonObject();
            logger.settings(accountKey + ".serverSettings", 900, "Email server config not found! Default values probably not applicable!");
            accountSettings.put("serverSettings", serverSettings);
        }
        if(!serverSettings.containsKey("emailAddress")) {
            logger.settings(accountKey + ".serverSettings.emailAddress", "test@localhost");
            serverSettings.put("emailAddress", "test@localhost");
        }
        if(!serverSettings.containsKey("smtpHost")) {
            logger.settings(accountKey + ".serverSettings.smtpHost", "localhost");
            serverSettings.put("smtpHost", "localhost");
        }
        if(!serverSettings.containsKey("smtpPort")) {
            logger.settings(accountKey + ".serverSettings.smtpPort", 25);
            serverSettings.put("smtpPort", 25);
        }
        if(!serverSettings.containsKey("imapHost")) {
            logger.settings(accountKey + ".serverSettings.imapHost", "localhost");
            serverSettings.put("imapHost", "localhost");
        }
        if(!serverSettings.containsKey("imapPort")) {
            logger.settings(accountKey + ".serverSettings.imapPort", 143);
            serverSettings.put("imapPort", 143);
        }
        if(!serverSettings.containsKey("user")) {
            logger.settings(accountKey + ".serverSettings.user", "testmail");
            serverSettings.put("user", "testmail");
        }
        if(!serverSettings.containsKey("password")) {
            logger.settings(accountKey + ".serverSettings.password", "test");
            serverSettings.put("password", "test");
        }
        if(!serverSettings.containsKey("isStartTLSRequired")) {
            logger.settings(accountKey + ".serverSettings.isStartTLSRequired", true);
            serverSettings.put("isStartTLSRequired", true);
        }
        if(!serverSettings.containsKey("isServerCertCheckRequired")) {
            logger.settings(accountKey + ".serverSettings.isServerCertCheckRequired", true);
            serverSettings.put("isServerCertCheckRequired", true);
        }

        // check login credentials
        Future<Boolean> credentialsFuture = Future.future();
        credentialsFuture.setHandler(credentialsResult -> {
            if(credentialsResult.failed()) {
                future.fail("Credentials seem to be invalid: " + credentialsResult.cause().getMessage());
                return;
            }


            // make sure the account is in the database
            Future<Void> ensureFuture = Future.future();
            ensureFuture.setHandler(ensureResult -> {
                if(ensureResult.failed()) {
                    future.fail("Ensuring database entry failed: " + ensureResult.cause().getMessage());
                    return;
                }

                accountConfigMap.put(accountKey, accountSettings);


                Pattern defaultEmailPattern = Pattern.compile(accountSettings.getJsonObject("sendingSettings").getString("emailWhitelistPattern"), Pattern.CASE_INSENSITIVE);
                emailWhitelistPatternMap.put(accountKey, defaultEmailPattern);

                // configure sending mails
                if(accountSettings.getBoolean("isSendingEnabled")) {
                    MailConfig config = new MailConfig();
                    config.setHostname(accountSettings.getJsonObject("serverSettings").getString("smtpHost"));
                    config.setPort(accountSettings.getJsonObject("serverSettings").getInteger("smtpPort"));
                    config.setUsername(accountSettings.getJsonObject("serverSettings").getString("user"));
                    config.setPassword(accountSettings.getJsonObject("serverSettings").getString("password"));

                    if (accountSettings.getJsonObject("serverSettings").getBoolean("isStartTLSRequired")) {
                        config.setStarttls(StartTLSOptions.REQUIRED);
                    } else {
                        config.setStarttls(StartTLSOptions.OPTIONAL);
                        logger.security(serviceID, 900, "Mail service transmits CREDENTIALS UNENCRYPTED - except in local testing context THIS IS A SEVERE SECURITY RISK!");
                    }

                    if (accountSettings.getJsonObject("serverSettings").getBoolean("isServerCertCheckRequired")) {
                        future.fail("Checking server cert not yet implemented!");
                        return;
                    }
                    else {
                        config.setTrustAll(true);
                    }

                    config.setAllowRcptErrors(accountSettings.getJsonObject("sendingSettings").getBoolean("isAllowRcptErrors"));
                    config.setOwnHostname(accountSettings.getJsonObject("sendingSettings").getString("ehloHostname"));

                    MailClient mailClient = MailClient.createNonShared(vertx, config);
                    mailClientMap.put(accountKey, mailClient);
                }

                // prepare monitoring
                Future<Boolean> monitoringFuture = Future.future();
                monitoringFuture.setHandler(monitoringResult -> {
                    if(monitoringResult.failed()) {
                        future.fail("Failed to prepare monitoring of IMAP folder" + monitoringResult.cause().getMessage());
                        return;
                    }
                    future.complete();
                });
                if(accountSettings.getBoolean("isMonitoringEnabled")) {
                    prepareMonitoringOfImapFolder(serviceID, accountKey, accountSettings.getJsonObject("monitoringSettings").getString("folderName"), accountSettings.getJsonObject("monitoringSettings").getJsonArray("notificationAddresses"), monitoringFuture);
                }
                else {
                    monitoringFuture.complete(true);
                }
            });
            ensureAccountDocumentEntry(accountKey, ensureFuture);
        });
        if(accountSettings.getBoolean("isMonitoringEnabled")
                || accountSettings.getBoolean("isSendingEnabled")) {
            verifyServerCredentials(serviceID, accountSettings.getJsonObject("serverSettings"), false, credentialsFuture);
        }
        else {
            credentialsFuture.complete(true);
        }
    }

    @Override
    public void prepare(Future<Object> prepareFuture) {

        accountConfigMap = new ConcurrentHashMap<>();
        mailClientMap = new ConcurrentHashMap<>();
        monitorFolderMap = new ConcurrentHashMap<>();
        monitorConfigMap = new ConcurrentHashMap<>();
        emailWhitelistPatternMap = new ConcurrentHashMap<>();

        if(!config().containsKey("isAddingAccountsEnabled")) {
            logger.settings("isAddingAccountsEnabled", false);
            config().put("isAddingAccountsEnabled", false);
        }
        JsonObject addingAccountSettings = config().getJsonObject("addingAccountSettings");
        if(addingAccountSettings == null) {
            addingAccountSettings = new JsonObject();
            logger.settings("addingAccountSettings", addingAccountSettings);
            config().put("addingAccountSettings", addingAccountSettings);
        }

        JsonArray whitelistByServiceOrigin = addingAccountSettings.getJsonArray("whitelistByServiceOrigin");
        if(whitelistByServiceOrigin == null) {
            whitelistByServiceOrigin = new JsonArray();
            logger.settings("addingAccountSettings.whitelistByServiceOrigin", whitelistByServiceOrigin);
            addingAccountSettings.put("addingAccountSettings.whitelistByServiceOrigin", whitelistByServiceOrigin);
        }

        JsonObject defaultAccount = config().getJsonObject("defaultAccount");
        if(defaultAccount != null && !defaultAccount.isEmpty()) {
            defaultAccount.put("accountKey", "defaultAccount");
            setupAccount(defaultAccount, prepareFuture);
        }
        else {
            prepareFuture.complete();
        }
    }

    @Override
    public void startConsuming() {
        if(config().getJsonObject("defaultAccount").getBoolean("isMonitoringEnabled")) {
            String monitorKey = config().getJsonObject("defaultAccount").getJsonObject("monitoringSettings").getString("folderName") + "@defaultAccount";
            Future<Boolean> startFuture = Future.future();
            startFuture.setHandler(startResult -> {
               if(startResult.failed()) {
                   logger.fatal(serviceID, 500, "Failed to start monitoring of imap folder!", startResult.cause());
               }
            });
            startMonitoringOfImapFolder(serviceID, monitorKey, startFuture);
        }

        // TODO: start periodic canary check for monitors
    }

    @Override
    public void shutdown(Future<Object> shutdownFuture) {
        if(monitorFolderMap != null && !mailClientMap.isEmpty()) {
            monitorFolderMap.forEach((key, object) -> {
                IMAPFolder folder = (IMAPFolder) object;
                monitorFolderMap.remove(key);
                try {
                    if (folder.isOpen()) {
                        folder.close();
                    }
                }
                catch (MessagingException e) {
                }
                try {
                    folder.getStore().close();
                }
                catch (MessagingException e) {
                }
            });
        }

        if(mailClientMap != null && !mailClientMap.isEmpty()) {
            mailClientMap.forEach((key, mailClient) -> {
                mailClient.close();
            });
        }

        shutdownFuture.complete();
    }
}

// disabling idle mode for now. Requires too many threads and/or resources. Falling back to pulling
    /*
    // this implementation might be a huge resource drainer - to be reworked, e.g. via canary
    private void monitorImapFolder(String correlationID, String monitorKey) {
        String cid = correlationID == null? UUID.randomUUID().toString() : correlationID;
        JsonObject monitorConfig = monitorConfigMap.get(monitorKey);
        IMAPFolder imapFolder = monitorFolderMap.get(monitorKey);

        if(monitorConfig == null || monitorConfig.isEmpty()) {
            logger.error(cid, 500, "'monitorConfig' with key " + monitorKey + " not found or empty!");
            return;
        }
        if(imapFolder == null) {
            logger.error(cid, 500, "'imapFolder' with key " + monitorKey + " not found!");
            return;
        }

        boolean isIdleSupportedByServer = monitorConfig.getBoolean("isIdleUsageEnabled");
        if(isIdleSupportedByServer) {
            try {
                imapFolder.idle();
                isIdleSupportedByServer = true;
            }
            catch (FolderClosedException e) {
                Long periodicID = vertx.setTimer(monitorConfig.getLong("errorRetryPauseInMS"), handler -> {
                    vertx.executeBlocking(retryFuture -> {
                        monitorImapFolder(cid, monitorKey);
                    }, false, retryResult -> {});
                });
                monitorConfig.put("periodicID", periodicID);
                return;
            }
            catch (MessagingException e) {
                isIdleSupportedByServer = false;
            }
        }

        if(isIdleSupportedByServer) {
            // loop for returning after processing an event
            boolean isIdleMonitoringRunning = true;
            while(isIdleMonitoringRunning) {
                try {
                    imapFolder.idle();
                }
                catch (FolderClosedException e) {
                    // break the loop
                    isIdleMonitoringRunning = false;

                    if(monitorFolderMap.containsKey(monitorKey)) {
                        // this is not a intended shutdown - restart
                        monitorFolderMap.remove(monitorKey);
                        Long periodicID = vertx.setTimer(monitorConfig.getLong("errorRetryPauseInMS"), retryHandler -> {
                            setupMonitoringImapFolder(cid, monitorConfig.getString("accountKey"), monitorConfig.getString("folderName"), monitorConfig.getJsonArray("notificationAddresses"), Future.future());
                        });
                        monitorConfig.put("periodicID", periodicID);
                    }
                }
                catch (Exception e) {
                    // TODO: use circuit...
                    // something went wrong, try again after a short delay
                    Long periodicID = vertx.setTimer(monitorConfig.getLong("errorRetryPauseInMS"), handler -> {
                        vertx.executeBlocking(retryFuture -> {
                            monitorImapFolder(cid, monitorKey);
                        }, retryResult -> {});
                    });
                    monitorConfig.put("periodicID", periodicID);
                    logger.warn(cid, 500, "Exception while monitoring " + monitorKey + ". Retrying in " + monitorConfig.getLong("errorRetryPauseInMS") + "ms");
                }
            }
        }
        else {
            Long periodicID = vertx.setPeriodic(monitorConfig.getLong("fallbackPullInMS"), handler -> {
                // force pull
                try {
                    imapFolder.getMessageCount();
                }
                catch (FolderClosedException e) {
                    Long currentPeriodicID = monitorConfig.getLong("periodicID");
                    if(currentPeriodicID != null) {
                        vertx.cancelTimer(currentPeriodicID);
                    }

                    if(monitorFolderMap.containsKey(monitorKey)) {
                        // this is not a intended shutdown - restart
                        logger.warn(cid, 500, "Exception while monitoring " + monitorKey + " via pull. Retrying in " + monitorConfig.getLong("errorRetryPauseInMS") + "ms");
                        monitorFolderMap.remove(monitorKey);
                        currentPeriodicID = vertx.setTimer(monitorConfig.getLong("errorRetryPauseInMS"), retryHandler -> {
                            setupMonitoringImapFolder(cid, monitorConfig.getString("accountKey"), monitorConfig.getString("folderName"), monitorConfig.getJsonArray("notificationAddresses"), Future.future());
                        });
                        monitorConfig.put("periodicID", currentPeriodicID);
                    }
                }
                catch (Exception e) {
                    Long currentPeriodicID = monitorConfig.getLong("periodicID");
                    if(currentPeriodicID != null) {
                        vertx.cancelTimer(currentPeriodicID);
                    }

                    if(monitorFolderMap.containsKey(monitorKey)) {
                        // this is not a intended shutdown - restart
                        logger.error(cid, 500, "Exception while monitoring " + monitorKey + " via pull. Retrying in " + monitorConfig.getLong("errorRetryPauseInMS") + "ms");
                        monitorFolderMap.remove(monitorKey);
                        currentPeriodicID = vertx.setTimer(monitorConfig.getLong("errorRetryPauseInMS"), retryHandler -> {
                            setupMonitoringImapFolder(cid, monitorConfig.getString("accountKey"), monitorConfig.getString("folderName"), monitorConfig.getJsonArray("notificationAddresses"), Future.future());
                        });
                        monitorConfig.put("periodicID", currentPeriodicID);
                    }
                }
            });
            monitorConfig.put("periodicID", periodicID);
        }
    }
    */