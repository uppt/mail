package de.maltebehrendt.uppt.services;

import de.maltebehrendt.uppt.InstanceRunner;
import de.maltebehrendt.uppt.junit.AbstractTest;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.Message;
import io.vertx.core.eventbus.MessageConsumer;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.joda.time.DateTime;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.lang.invoke.MethodHandles;
import java.util.UUID;

public class MailService1Test extends AbstractTest {

    @BeforeClass
    public static void setup(TestContext context) {
        setupTestVertxAndDatabase(MethodHandles.lookup().lookupClass().getSimpleName(), context);
    }

    @AfterClass
    public static void teardown(TestContext context) {
        tearDownTestVertxAndCleanupDatabase(MethodHandles.lookup().lookupClass().getSimpleName(), context);
    }

    @Test
    public void testSimpleUnsendTextMailWithoutCheckingArrival(TestContext context) {
        Async requestResult = context.async(1);

        DeliveryOptions options = new DeliveryOptions()
                .addHeader("statusCode", "200")
                .addHeader("correlationID", "456456")
                .addHeader("origin", "testSimpleUnsendTextMailWithoutCheckingArrival");

        eventBus.send("mails.1.unsendMessage", new JsonObject()
                .put("from","testmail@localhost")
                .put("to", new JsonArray().add("testmail@localhost"))
                .put("cc", new JsonArray())
                .put("bcc", new JsonArray())
                .put("subject","Test mail")
                .put("text","This is only a test")
                .put("html","")
                , options, reply -> {

            context.assertTrue(reply.succeeded());

            JsonObject replyMessage = (JsonObject) reply.result().body();

            String statusCode = reply.result().headers().get("statusCode");
            context.assertEquals("200", statusCode);
            context.assertNotNull(replyMessage);
            context.assertNotNull(replyMessage.getString("id"));
            context.assertFalse(replyMessage.getString("id").isEmpty());
            context.assertNotNull(replyMessage.getJsonArray("recipients"));
            context.assertFalse(replyMessage.getJsonArray("recipients").isEmpty());
            context.assertEquals(1, replyMessage.getJsonArray("recipients").size());
            context.assertEquals("testmail@localhost", replyMessage.getJsonArray("recipients").getString(0));
            requestResult.countDown();
        });


        requestResult.awaitSuccess();
    }

    @Test
    public void testSimpleUnsendTextMailWithNewMessageMonitor(TestContext context) {
        Async requestResult = context.async(4);

        String from = "testmail@localhost";
        String text = "This is only a test";
        String uuid1 = UUID.randomUUID().toString();
        String subject1 = "Test mail 1: " + uuid1;
        String uuid2 = UUID.randomUUID().toString();
        String subject2 = "Test mail 2: " + uuid2;

        MessageConsumer<JsonObject> consumer = eventBus.consumer("mails.1.newMessage");
        consumer.handler(new Handler<Message<JsonObject>>() {
            @Override
            public void handle(io.vertx.core.eventbus.Message<JsonObject> message) {
                de.maltebehrendt.uppt.events.Impl.MessageImpl upptMessageImpl = new de.maltebehrendt.uppt.events.Impl.MessageImpl(message);
                JsonObject payload = upptMessageImpl.getBodyAsJsonObject();

                String subject = payload.getString("subject");
                DateTime time = new DateTime();

                if(subject1.equalsIgnoreCase(subject)) {
                    context.assertEquals(from, payload.getString("from"));
                    context.assertNotNull(payload.getInteger("id"));
                    context.assertTrue(payload.getInteger("id") > 0);
                    context.assertNotNull(payload.getLong("receivedTimestamp"));
                    context.assertTrue(time.minusSeconds(10).getMillis() < payload.getLong("receivedTimestamp"));
                    requestResult.countDown();
                }
                else if(subject2.equalsIgnoreCase(subject)) {
                    context.assertEquals(from, payload.getString("from"));
                    context.assertNotNull(payload.getInteger("id"));
                    context.assertTrue(payload.getInteger("id") > 0);
                    context.assertNotNull(payload.getLong("receivedTimestamp"));
                    context.assertTrue(time.minusSeconds(10).getMillis() < payload.getLong("receivedTimestamp"));
                    requestResult.countDown();
                }
            }
        });
        context.assertTrue(consumer.isRegistered(), "Mockup new message processor is registered");

        DeliveryOptions options = new DeliveryOptions()
                .addHeader("statusCode", "200")
                .addHeader("correlationID", "456456")
                .addHeader("origin", "testSimpleUnsendTextMailWithNewMessageMonitor");

        eventBus.send("mails.1.unsendMessage", new JsonObject()
                        .put("from", from)
                        .put("to", new JsonArray().add("testmail@localhost"))
                        .put("cc", new JsonArray())
                        .put("bcc", new JsonArray())
                        .put("subject", subject1)
                        .put("text", text)
                        .put("html","")
                , options, reply -> {

                    context.assertTrue(reply.succeeded());
                    String statusCode = reply.result().headers().get("statusCode");
                    context.assertEquals("200", statusCode);
                    requestResult.countDown();
                });

        eventBus.send("mails.1.unsendMessage", new JsonObject()
                        .put("from", from)
                        .put("to", new JsonArray().add("testmail@localhost"))
                        .put("cc", new JsonArray())
                        .put("bcc", new JsonArray())
                        .put("subject", subject2)
                        .put("text", text)
                        .put("html","")
                , options, reply -> {

                    context.assertTrue(reply.succeeded());
                    String statusCode = reply.result().headers().get("statusCode");
                    context.assertEquals("200", statusCode);
                    requestResult.countDown();
                });

        requestResult.awaitSuccess();
        consumer.unregister();
    }


    @Test
    public void testRetrieveSimpleTextMail(TestContext context) {
        Async requestResult = context.async(3);

        String from = "testmail@localhost";
        String text = "This is only a test";
        String uuid3 = UUID.randomUUID().toString();
        String subject3 = "Test mail 3: " + uuid3;

        DeliveryOptions options = new DeliveryOptions()
                .addHeader("statusCode", "200")
                .addHeader("correlationID", "465456456")
                .addHeader("origin", "testRetrieveSimpleTextMail");

        MessageConsumer<JsonObject> consumer = eventBus.consumer("mails.1.newMessage");
        consumer.handler(new Handler<Message<JsonObject>>() {
            @Override
            public void handle(io.vertx.core.eventbus.Message<JsonObject> message) {
                de.maltebehrendt.uppt.events.Impl.MessageImpl upptMessageImpl = new de.maltebehrendt.uppt.events.Impl.MessageImpl(message);
                JsonObject payload = upptMessageImpl.getBodyAsJsonObject();

                String subject = payload.getString("subject");

                if(subject3.equalsIgnoreCase(subject)) {
                    Integer id = payload.getInteger("id");
                    context.assertNotNull(id);
                    context.assertTrue(id > 0);
                    requestResult.countDown();

                    eventBus.send("mails.1.missingMessage", new JsonObject()
                                    .put("id", id)
                            , options, reply -> {
                                context.assertTrue(reply.succeeded());
                                String statusCode = reply.result().headers().get("statusCode");
                                context.assertEquals("200", statusCode);
                                JsonObject retrievedMessage = (JsonObject) reply.result().body();
                                context.assertNotNull(retrievedMessage);
                                context.assertEquals(from, retrievedMessage.getString("from"));
                                context.assertEquals(subject3, retrievedMessage.getString("subject"));
                                context.assertEquals(text, retrievedMessage.getString("text"));
                                context.assertEquals(from, retrievedMessage.getString("replyTo"));
                                context.assertEquals("", retrievedMessage.getString("notes"));
                                context.assertTrue(retrievedMessage.getString("contentType").contains("text/plain"));
                                context.assertNotNull(retrievedMessage.getLong("sentTimestamp"));
                                context.assertNotNull(retrievedMessage.getLong("receivedTimestamp"));
                                context.assertNotNull(retrievedMessage.getJsonArray("cc"));
                                context.assertNotNull(retrievedMessage.getJsonArray("bcc"));

                                requestResult.countDown();
                            });
                }
            }
        });
        context.assertTrue(consumer.isRegistered(), "Mockup new message processor is registered");

        eventBus.send("mails.1.unsendMessage", new JsonObject()
                        .put("from", from)
                        .put("to", new JsonArray().add("testmail@localhost"))
                        .put("cc", new JsonArray())
                        .put("bcc", new JsonArray())
                        .put("subject", subject3)
                        .put("text", text)
                        .put("html","")
                , options, reply -> {

                    context.assertTrue(reply.succeeded());
                    String statusCode = reply.result().headers().get("statusCode");
                    context.assertEquals("200", statusCode);
                    requestResult.countDown();
                });

        requestResult.awaitSuccess();
    }
}
