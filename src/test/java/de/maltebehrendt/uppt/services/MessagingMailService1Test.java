package de.maltebehrendt.uppt.services;

import de.maltebehrendt.uppt.junit.AbstractTest;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.lang.invoke.MethodHandles;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class MessagingMailService1Test extends AbstractTest {
    @BeforeClass
    public static void setup(TestContext context) {
        setupTestVertxAndDatabase(MethodHandles.lookup().lookupClass().getSimpleName(), context);
    }

    @AfterClass
    public static void teardown(TestContext context) {
        //tearDownTestVertxAndCleanupDatabase(MethodHandles.lookup().lookupClass().getSimpleName(), context);
    }

    @Test
    public void testEmailCreatedMinimum(TestContext context) {
        Async requestResult = context.async(4);

        JsonArray messageIDs = new JsonArray();

        send("messagingEmail", "1", "emailCreated", new JsonObject()
                    .put("envelope", new JsonObject()
                        .put("toArray", new JsonArray().add("testmail@localhost"))
                        .put("subject", "Test mail 0"))
                    .put("payload", new JsonObject()
                        .put("text", "This is only a test"))
                , reply -> {
            context.assertTrue(reply.succeeded());
            JsonObject result = reply.getBodyAsJsonObject();

            context.assertNotNull(result);
            context.assertNotNull(result.getString("messageID"));
            context.assertNotNull(result.getString("emailDocumentKey"));
            context.assertNotNull(result.getString("threadDocumentKey"));
            context.assertNotNull(result.getString("correspondenceDocumentKey"));
            context.assertNotNull(result.getJsonArray("recipients"));
            context.assertFalse(result.getJsonArray("recipients").isEmpty());
            context.assertNotNull(result.getJsonArray("recipients").getString(0));

            String threadKey = result.getString("threadDocumentKey");
            String correspondenceDocumentKey = result.getString("correspondenceDocumentKey");
            messageIDs.add(result.getString("messageID"));

            requestResult.countDown();

            send("messagingEmail", "1", "emailsOfAccountRequired", new JsonObject()
                            .put("maxMails", 150)
                    , retrieveReply -> {
                        context.assertTrue(retrieveReply.succeeded());
                        JsonArray mails = retrieveReply.getBodyAsJsonObject().getJsonArray("mails");

                        context.assertNotNull(mails);
                        context.assertFalse(mails.isEmpty());

                        int foundMessageIDs = 0;
                        for(int i=0;i<mails.size() && foundMessageIDs < messageIDs.size();i++) {
                            JsonObject mail = mails.getJsonObject(i);

                            if(messageIDs.contains(mail.getString("messageID"))) {
                                foundMessageIDs++;
                            }
                        }
                        context.assertEquals(messageIDs.size(), foundMessageIDs, "All messages are retrievable by account");
                        requestResult.countDown();
                    });

            send("messagingEmail", "1", "emailsOfThreadRequired", new JsonObject()
                            .put("threadDocumentKey", threadKey)
                    , retrieveReply -> {
                        context.assertTrue(retrieveReply.succeeded());
                        JsonArray mails = retrieveReply.getBodyAsJsonObject().getJsonArray("mails");

                        context.assertNotNull(mails);
                        context.assertFalse(mails.isEmpty());

                        int foundMessageIDs = 0;
                        for(int i=0;i<mails.size() && foundMessageIDs < messageIDs.size();i++) {
                            JsonObject mail = mails.getJsonObject(i);

                            if(messageIDs.contains(mail.getString("messageID"))) {
                                foundMessageIDs++;
                            }
                        }
                        context.assertEquals(messageIDs.size(), foundMessageIDs, "All messages are retrievable by thread");
                        requestResult.countDown();
                    });

            send("messagingEmail", "1", "emailsOfCorrespondentsRequired", new JsonObject()
                            .put("correspondenceDocumentKey", correspondenceDocumentKey)
                            .put("maxMails", 150)
                    , retrieveReply -> {
                        context.assertTrue(retrieveReply.succeeded());
                        JsonArray mails = retrieveReply.getBodyAsJsonObject().getJsonArray("mails");

                        context.assertNotNull(mails);
                        context.assertFalse(mails.isEmpty());

                        int foundMessageIDs = 0;
                        for(int i=0;i<mails.size() && foundMessageIDs < messageIDs.size();i++) {
                            JsonObject mail = mails.getJsonObject(i);

                            if(messageIDs.contains(mail.getString("messageID"))) {
                                foundMessageIDs++;
                            }
                        }
                        context.assertEquals(messageIDs.size(), foundMessageIDs, "All messages are retrievable by account");
                        requestResult.countDown();
                    });
        });

        requestResult.awaitSuccess();
    }

    @Test
    public void testEmailCreatedMinimumWithExistingThread(TestContext context) {
        Async requestResult = context.async(5);

        JsonArray messageIDs = new JsonArray();

        send("messagingEmail", "1", "emailCreated", new JsonObject()
                        .put("envelope", new JsonObject()
                                .put("toArray", new JsonArray().add("testmail@localhost"))
                                .put("subject", "Test mail 1"))
                        .put("payload", new JsonObject()
                                .put("text", "This is only a test"))
                , reply -> {
                    context.assertTrue(reply.succeeded());
                    JsonObject result = reply.getBodyAsJsonObject();

                    context.assertNotNull(result);
                    context.assertNotNull(result.getString("messageID"));
                    context.assertNotNull(result.getString("emailDocumentKey"));
                    context.assertNotNull(result.getString("threadDocumentKey"));
                    context.assertNotNull(result.getString("correspondenceDocumentKey"));
                    context.assertNotNull(result.getJsonArray("recipients"));
                    context.assertFalse(result.getJsonArray("recipients").isEmpty());
                    context.assertNotNull(result.getJsonArray("recipients").getString(0));

                    String messageID = result.getString("messageID");
                    String threadDocumentKey1 = result.getString("threadDocumentKey");
                    String threadKey = result.getString("threadDocumentKey");
                    String correspondenceDocumentKey = result.getString("correspondenceDocumentKey");
                    messageIDs.add(result.getString("messageID"));
                    requestResult.countDown();

                    send("messagingEmail", "1", "emailCreated", new JsonObject()
                                    .put("envelope", new JsonObject()
                                            .put("toArray", new JsonArray().add("testmail@localhost"))
                                            .put("subject", "Test mail 2")
                                            .put("inReplyTo", messageID))
                                    .put("payload", new JsonObject()
                                            .put("text", "This is only a test"))
                            , reply2 -> {
                                context.assertTrue(reply2.succeeded());
                                JsonObject result2 = reply2.getBodyAsJsonObject();

                                context.assertNotNull(result2);
                                context.assertNotNull(result2.getString("messageID"));
                                context.assertNotNull(result2.getString("emailDocumentKey"));
                                context.assertNotNull(result2.getString("threadDocumentKey"));
                                context.assertNotNull(result2.getString("correspondenceDocumentKey"));
                                context.assertNotNull(result2.getJsonArray("recipients"));
                                context.assertFalse(result2.getJsonArray("recipients").isEmpty());
                                context.assertNotNull(result2.getJsonArray("recipients").getString(0));

                                String threadDocumentKey2 = result2.getString("threadDocumentKey");
                                context.assertEquals(threadDocumentKey1, threadDocumentKey2);

                                messageIDs.add(result2.getString("messageID"));

                                requestResult.countDown();

                                send("messagingEmail", "1", "emailCreated", new JsonObject()
                                                .put("envelope", new JsonObject()
                                                        .put("toArray", new JsonArray().add("testmail@localhost"))
                                                        .put("subject", "Test mail 3"))
                                                .put("payload", new JsonObject()
                                                        .put("text", "This is only a test"))
                                                .put("threadDocumentKey", threadDocumentKey2)
                                        , reply3 -> {
                                            context.assertTrue(reply3.succeeded());
                                            JsonObject result3 = reply3.getBodyAsJsonObject();

                                            context.assertNotNull(result3);
                                            context.assertNotNull(result3.getString("messageID"));
                                            context.assertNotNull(result3.getString("emailDocumentKey"));
                                            context.assertNotNull(result3.getString("threadDocumentKey"));
                                            context.assertNotNull(result3.getString("correspondenceDocumentKey"));
                                            context.assertNotNull(result3.getJsonArray("recipients"));
                                            context.assertFalse(result3.getJsonArray("recipients").isEmpty());
                                            context.assertNotNull(result3.getJsonArray("recipients").getString(0));

                                            String threadDocumentKey3 = result3.getString("threadDocumentKey");

                                            context.assertEquals(threadDocumentKey1, threadDocumentKey3);
                                            messageIDs.add(result3.getString("messageID"));

                                            requestResult.countDown();

                                            send("messagingEmail", "1", "emailsOfAccountRequired", new JsonObject()
                                                            .put("maxMails", 150)
                                                    , retrieveReply -> {
                                                        context.assertTrue(retrieveReply.succeeded());
                                                        JsonArray mails = retrieveReply.getBodyAsJsonObject().getJsonArray("mails");

                                                        context.assertNotNull(mails);
                                                        context.assertFalse(mails.isEmpty());

                                                        int foundMessageIDs = 0;
                                                        for(int i=0;i<mails.size() && foundMessageIDs < messageIDs.size();i++) {
                                                            JsonObject mail = mails.getJsonObject(i);

                                                            if(messageIDs.contains(mail.getString("messageID"))) {
                                                                foundMessageIDs++;
                                                            }
                                                        }
                                                        context.assertEquals(messageIDs.size(), foundMessageIDs, "All messages are retrievable by account");
                                                        requestResult.countDown();
                                                    });

                                            send("messagingEmail", "1", "emailsOfThreadRequired", new JsonObject()
                                                            .put("threadDocumentKey", threadKey)
                                                    , retrieveReply -> {
                                                        context.assertTrue(retrieveReply.succeeded());
                                                        JsonArray mails = retrieveReply.getBodyAsJsonObject().getJsonArray("mails");

                                                        context.assertNotNull(mails);
                                                        context.assertFalse(mails.isEmpty());

                                                        int foundMessageIDs = 0;
                                                        for(int i=0;i<mails.size() && foundMessageIDs < messageIDs.size();i++) {
                                                            JsonObject mail = mails.getJsonObject(i);

                                                            if(messageIDs.contains(mail.getString("messageID"))) {
                                                                foundMessageIDs++;
                                                            }
                                                        }
                                                        context.assertEquals(messageIDs.size(), foundMessageIDs, "All messages are retrievable by thread");
                                                        requestResult.countDown();
                                                    });

                                            send("messagingEmail", "1", "emailsOfCorrespondentsRequired", new JsonObject()
                                                            .put("correspondenceDocumentKey", correspondenceDocumentKey)
                                                            .put("maxMails", 150)
                                                    , retrieveReply -> {
                                                        context.assertTrue(retrieveReply.succeeded());
                                                        JsonArray mails = retrieveReply.getBodyAsJsonObject().getJsonArray("mails");

                                                        context.assertNotNull(mails);
                                                        context.assertFalse(mails.isEmpty());

                                                        int foundMessageIDs = 0;
                                                        for(int i=0;i<mails.size() && foundMessageIDs < messageIDs.size();i++) {
                                                            JsonObject mail = mails.getJsonObject(i);

                                                            if(messageIDs.contains(mail.getString("messageID"))) {
                                                                foundMessageIDs++;
                                                            }
                                                        }
                                                        context.assertEquals(messageIDs.size(), foundMessageIDs, "All messages are retrievable by account");
                                                        requestResult.countDown();
                                                    });
                                        });
                            });
                });

        requestResult.awaitSuccess();
    }

    @Test
    public void testEmailCreatedMinimumWithUnknownInReplyTo(TestContext context) {
        Async requestResult = context.async(1);

        send("messagingEmail", "1", "emailCreated", new JsonObject()
                        .put("envelope", new JsonObject()
                                .put("toArray", new JsonArray().add("testmail@localhost"))
                                .put("subject", "Test mail 4")
                                .put("inReplyTo", UUID.randomUUID().toString()))
                        .put("payload", new JsonObject()
                                .put("text", "This is only a test"))
                , reply -> {
                    context.assertTrue(reply.succeeded());
                    JsonObject result = reply.getBodyAsJsonObject();

                    context.assertNotNull(result);
                    context.assertNotNull(result.getString("messageID"));
                    context.assertNotNull(result.getString("emailDocumentKey"));
                    context.assertNotNull(result.getString("threadDocumentKey"));
                    context.assertNotNull(result.getString("correspondenceDocumentKey"));
                    context.assertNotNull(result.getJsonArray("recipients"));
                    context.assertFalse(result.getJsonArray("recipients").isEmpty());
                    context.assertNotNull(result.getJsonArray("recipients").getString(0));

                    requestResult.countDown();
                });

        requestResult.awaitSuccess();
    }

    @Test
    public void testEmailCreatedMinimumWithMonitoringRecipientAccount(TestContext context) {
        Async requestResult = context.async(3);
        ConcurrentHashMap<String, String> messageIDMap = new ConcurrentHashMap<>();

        registerConsumer("testMessagingMail", "1", "testEmailCreatedMinimumWithMonitoringRecipientAccount", message -> {
            if(messageIDMap.containsKey(message.getBodyAsJsonObject().getString("messageID"))) {
                requestResult.countDown();
            }
        });


        JsonObject accountSettings = new JsonObject()
                .put("accountKey", "testmail1@localhost")
                .put("serverSettings", new JsonObject()
                    .put("emailAddress","testmail@localhost")
                    .put("isStartTLSRequired", false)
                    .put("isServerCertCheckRequired", false)
                )
                .put("isMonitoringEnabled", true)
                .put("monitoringSettings", new JsonObject()
                    .put("folderName", "Inbox")
                    .put("notificationAddresses", new JsonArray().add(new JsonObject()
                        .put("domain", "testMessagingMail")
                        .put("version", "1")
                        .put("type", "testEmailCreatedMinimumWithMonitoringRecipientAccount")))
                    .put("fallbackPullInMS", 1000)
                );

        send("messagingEmail", "1", "newAccountAvailable", accountSettings
                , reply -> {
                    context.assertTrue(reply.succeeded());

                    requestResult.countDown();

                    send("messagingEmail", "1", "emailCreated", new JsonObject()
                                    .put("envelope", new JsonObject()
                                            .put("toArray", new JsonArray().add("testmail@localhost"))
                                            .put("subject", "Test mail 5"))
                                    .put("payload", new JsonObject()
                                            .put("text", "This is only a test"))
                            , reply2 -> {
                                context.assertTrue(reply2.succeeded());
                                messageIDMap.put(reply2.getBodyAsJsonObject().getString("messageID"), reply2.getBodyAsJsonObject().getString("threadDocumentKey"));
                                requestResult.countDown();
                            });
                });


        requestResult.awaitSuccess();
    }

    @Test
    public void testEmailCreatedMinimumWithThreadsWithMonitoringRecipientAccount(TestContext context) {
        Async requestResult = context.async(7);

        ConcurrentHashMap<String, JsonObject> messageIDMap = new ConcurrentHashMap<>();
        ConcurrentHashMap<String, JsonObject> notificationMessageIDMap = new ConcurrentHashMap<>();

        registerConsumer("testMessagingMail", "1", "testEmailCreatedMinimumWithThreadsWithMonitoringRecipientAccount", message -> {
            JsonObject messageNotification = message.getBodyAsJsonObject();
            String messageID = messageNotification.getString("messageID");

            if (messageIDMap.containsKey(messageID)) {
                notificationMessageIDMap.put(messageNotification.getString("messageID"), messageNotification);
            }

            if(!messageIDMap.isEmpty() && notificationMessageIDMap.size() == 3) {
                JsonObject lastMessage = notificationMessageIDMap.get(messageID);
                String threadKey = lastMessage.getString("threadDocumentKey");
                String correspondenceDocumentKey = lastMessage.getString("correspondenceDocumentKey");

                send("messagingEmail", "1", "emailsOfAccountRequired", new JsonObject()
                                .put("accountKey", "testmail2@localhost")
                                .put("maxMails", 150)
                        , retrieveReply -> {
                            context.assertTrue(retrieveReply.succeeded());
                            JsonArray mails = retrieveReply.getBodyAsJsonObject().getJsonArray("mails");

                            context.assertNotNull(mails);
                            context.assertFalse(mails.isEmpty());

                            int foundMessageIDs = 0;
                            for(int i=0;i<mails.size() && foundMessageIDs < messageIDMap.size();i++) {
                                JsonObject mail = mails.getJsonObject(i);

                                if(notificationMessageIDMap.containsKey(mail.getString("messageID"))) {
                                    foundMessageIDs++;
                                    context.assertEquals("testmail2@localhost", mail.getString("accountKey"));
                                }

                            }
                            context.assertEquals(messageIDMap.size(), foundMessageIDs, "All messages are retrievable by account");
                            requestResult.countDown();
                        });

                send("messagingEmail", "1", "emailsOfThreadRequired", new JsonObject()
                                .put("accountKey", "testmail2@localhost")
                                .put("threadDocumentKey", threadKey)
                        , retrieveReply -> {
                            context.assertTrue(retrieveReply.succeeded());
                            JsonArray mails = retrieveReply.getBodyAsJsonObject().getJsonArray("mails");

                            context.assertNotNull(mails);
                            context.assertFalse(mails.isEmpty());


                            int foundMessageIDs = 0;
                            for(int i=0;i<mails.size() && foundMessageIDs < messageIDMap.size();i++) {
                                JsonObject mail = mails.getJsonObject(i);

                                if(notificationMessageIDMap.containsKey(mail.getString("messageID"))) {
                                    foundMessageIDs++;
                                    context.assertEquals("testmail2@localhost", mail.getString("accountKey"));
                                }
                            }
                            context.assertEquals(messageIDMap.size(), foundMessageIDs, "All messages are retrievable by thread");
                            requestResult.countDown();
                        });

                send("messagingEmail", "1", "emailsOfCorrespondentsRequired", new JsonObject()
                                .put("correspondenceDocumentKey", correspondenceDocumentKey)
                                .put("accountKey", "testmail2@localhost")
                                .put("maxMails", 150)
                        , retrieveReply -> {
                            context.assertTrue(retrieveReply.succeeded());
                            JsonArray mails = retrieveReply.getBodyAsJsonObject().getJsonArray("mails");

                            context.assertNotNull(mails);
                            context.assertFalse(mails.isEmpty());

                            int foundMessageIDs = 0;
                            for(int i=0;i<mails.size() && foundMessageIDs < messageIDMap.size();i++) {
                                JsonObject mail = mails.getJsonObject(i);

                                if(notificationMessageIDMap.containsKey(mail.getString("messageID"))) {
                                    foundMessageIDs++;
                                    context.assertEquals("testmail2@localhost", mail.getString("accountKey"));
                                }
                            }
                            context.assertEquals(messageIDMap.size(), foundMessageIDs, "All messages are retrievable by account");
                            requestResult.countDown();
                        });

                messageIDMap.clear();
            }
        });


        JsonObject accountSettings = new JsonObject()
                .put("accountKey", "testmail2@localhost")
                .put("serverSettings", new JsonObject()
                        .put("emailAddress","testmail@localhost")
                        .put("isStartTLSRequired", false)
                        .put("isServerCertCheckRequired", false)
                )
                .put("isMonitoringEnabled", true)
                .put("monitoringSettings", new JsonObject()
                        .put("folderName", "Inbox")
                        .put("whitelistByServiceOrigin", new JsonArray().add("*"))
                        .put("notificationAddresses", new JsonArray().add(new JsonObject()
                                .put("domain", "testMessagingMail")
                                .put("version", "1")
                                .put("type", "testEmailCreatedMinimumWithThreadsWithMonitoringRecipientAccount")))
                        .put("fallbackPullInMS", 1000)
                );

        send("messagingEmail", "1", "newAccountAvailable", accountSettings
                , accountReply -> {
                    context.assertTrue(accountReply.succeeded());
                    requestResult.countDown();

                    send("messagingEmail", "1", "emailCreated", new JsonObject()
                                    .put("envelope", new JsonObject()
                                            .put("toArray", new JsonArray().add("testmail@localhost"))
                                            .put("subject", "Test mail 1"))
                                    .put("payload", new JsonObject()
                                            .put("text", "This is only a test"))
                            , reply -> {
                                context.assertTrue(reply.succeeded());
                                JsonObject result = reply.getBodyAsJsonObject();

                                context.assertNotNull(result);
                                context.assertNotNull(result.getString("messageID"));
                                context.assertNotNull(result.getString("emailDocumentKey"));
                                context.assertNotNull(result.getString("threadDocumentKey"));
                                context.assertNotNull(result.getString("correspondenceDocumentKey"));
                                context.assertNotNull(result.getJsonArray("recipients"));
                                context.assertFalse(result.getJsonArray("recipients").isEmpty());
                                context.assertNotNull(result.getJsonArray("recipients").getString(0));

                                String messageID = result.getString("messageID");
                                String threadDocumentKey1 = result.getString("threadDocumentKey");
                                messageIDMap.put(result.getString("messageID"), result);
                                requestResult.countDown();

                                send("messagingEmail", "1", "emailCreated", new JsonObject()
                                                .put("envelope", new JsonObject()
                                                        .put("toArray", new JsonArray().add("testmail@localhost"))
                                                        .put("subject", "Test mail 2")
                                                        .put("inReplyTo", messageID))
                                                .put("payload", new JsonObject()
                                                        .put("text", "This is only a test"))
                                        , reply2 -> {
                                            context.assertTrue(reply2.succeeded());
                                            JsonObject result2 = reply2.getBodyAsJsonObject();

                                            context.assertNotNull(result2);
                                            context.assertNotNull(result2.getString("messageID"));
                                            context.assertNotNull(result2.getString("emailDocumentKey"));
                                            context.assertNotNull(result2.getString("threadDocumentKey"));
                                            context.assertNotNull(result2.getString("correspondenceDocumentKey"));
                                            context.assertNotNull(result2.getJsonArray("recipients"));
                                            context.assertFalse(result2.getJsonArray("recipients").isEmpty());
                                            context.assertNotNull(result2.getJsonArray("recipients").getString(0));

                                            String threadDocumentKey2 = result2.getString("threadDocumentKey");
                                            context.assertEquals(threadDocumentKey1, threadDocumentKey2);

                                            messageIDMap.put(result2.getString("messageID"), result2);

                                            requestResult.countDown();

                                            send("messagingEmail", "1", "emailCreated", new JsonObject()
                                                            .put("envelope", new JsonObject()
                                                                    .put("toArray", new JsonArray().add("testmail@localhost"))
                                                                    .put("subject", "Test mail 3")
                                                                    .put("inReplyTo", messageID))
                                                            .put("payload", new JsonObject()
                                                                    .put("text", "This is only a test"))
                                                            //.put("threadDocumentKey", threadDocumentKey2)
                                                    , reply3 -> {
                                                        context.assertTrue(reply3.succeeded());
                                                        JsonObject result3 = reply3.getBodyAsJsonObject();

                                                        context.assertNotNull(result3);
                                                        context.assertNotNull(result3.getString("messageID"));
                                                        context.assertNotNull(result3.getString("emailDocumentKey"));
                                                        context.assertNotNull(result3.getString("threadDocumentKey"));
                                                        context.assertNotNull(result3.getString("correspondenceDocumentKey"));
                                                        context.assertNotNull(result3.getJsonArray("recipients"));
                                                        context.assertFalse(result3.getJsonArray("recipients").isEmpty());
                                                        context.assertNotNull(result3.getJsonArray("recipients").getString(0));

                                                        String threadDocumentKey3 = result3.getString("threadDocumentKey");

                                                        context.assertEquals(threadDocumentKey1, threadDocumentKey3);
                                                        messageIDMap.put(result3.getString("messageID"), result3);
                                                        requestResult.countDown();
                                                    });
                                        });
                            });
                });


        requestResult.awaitSuccess();
    }



    // TODO: thread is not first
    // TODO: massive load (~100 msg)
}
